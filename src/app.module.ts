import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ErplyModule } from './modules/erply/erply.module';
import { SandboxModule } from './modules/sandbox/sandbox.module';
import { XmlBuilderModule } from './helpers/builders/xml/xml-builder.module';
import { SuppliersModule } from './modules/boms/suppliers/suppliers.module';
import { ScheduleModule } from '@nestjs/schedule';
import { AuthModule } from './modules/auth/auth.module';
import { SchedulesModule } from './schedules/schedules.module';
import { PurchaseOrdersModule } from './modules/boms/purchase-orders/purchase-orders.module';
import { PurchaseOrdersController } from './modules/boms/purchase-orders/purchase-orders.controller';
import { SftpModule } from './helpers/sftp/sftp.module';
import { SystemModule } from './modules/system/system.module';
import { MailModule } from './helpers/mail/mail.module';
import { FilesystemModule } from './helpers/filesystem/filesystem.module';
@Module({
  imports: [
    TypeOrmModule.forRoot(),
    ScheduleModule.forRoot(),
    ErplyModule,
    SandboxModule,
    XmlBuilderModule,
    SuppliersModule,
    PurchaseOrdersModule,
    AuthModule,
    SchedulesModule,
    SftpModule,
    SystemModule,
    MailModule,
    FilesystemModule,
  ],
  controllers: [AppController, PurchaseOrdersController],
  providers: [AppService],
})
export class AppModule { }
