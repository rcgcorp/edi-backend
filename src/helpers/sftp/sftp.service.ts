import { Injectable } from '@nestjs/common';

@Injectable()
export class SftpService {

    sftp: any;
    config: any;

    constructor() {
        let Client = require('ssh2-sftp-client');
        this.sftp = new Client();
        this.config = {
            host: process.env.SFTP_HOST,
            port: process.env.SFTP_PORT,
            username: process.env.SFTP_USER,
            password: process.env.SFTP_PASS
            // privateKey: require('fs').readFileSync(process.env.SFTP_KEY)
        }
    }

    init() {
        let Client = require('ssh2-sftp-client');
        let sftp = new Client();
        return sftp;
    }

    list() {
        this.sftp.connect(this.config)
            .then(() => {
                return this.sftp.list('/incoming/NZ_TEST');
            }).catch(err => {
                console.log(err, 'catch error');
            });
    }

    async put(filename: string, is_error?: boolean, directory?: string) {
        console.log("Putting file into SFTP")
        let sftp = this.init();
        let sftp_directory = process.env.SFTP_REMOTE_FOLDER;
        let source_path = directory ? directory + '/' + filename : `./temp/${filename}`
        let destination_path = `${is_error ? sftp_directory + '/error' : sftp_directory}/${filename}`
        await sftp.connect(this.config)
            .then(async () => {
                await sftp.put(source_path, destination_path);
                await sftp.end();
            })
            .catch(err => {
                console.log(err, 'catch error');
                console.log('error on putting file: ' + filename)
                sftp.end();
                throw err;
            });
    }

    async read(filename: string) {
        let fs = require('fs');
    }

    async get() {
        let fs = require('fs');
        let sftp = this.init();
        let sftp_directory = process.env.SFTP_REMOTE_FOLDER_GET;

        var dir = './temp/pending';
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir, { recursive: true });
        }
        // let source_path = `./temp/${filename}`
        // let destination_path = `${sftp_directory}/${filename}`
        console.log(sftp_directory);
        await sftp.connect(this.config)
            .then(async () => {
                let items = await sftp.list(process.env.SFTP_REMOTE_FOLDER_GET);
                console.log(items);
                let file_names = items.filter(item => item.type != 'd').map(item => {
                    return {
                        remote_path: `${sftp_directory}/${item.name}`,
                        destination_path: `./temp/pending/${item.name}`,
                        archive_path: `${sftp_directory}/archive/${item.name}`,
                    }
                })
                console.log(file_names);
                let get_promises = [];
                let rename_promises = [];

                if (file_names.length > 0) {
                    let files_to_get = file_names.length > 20 ? 20 : file_names.length
                    for (let i = 0; i < files_to_get; i++) {
                        let file = file_names[i];
                        get_promises.push(sftp.get(file.remote_path, fs.createWriteStream(file.destination_path)))
                        rename_promises.push(sftp.rename(file.remote_path, file.archive_path));
                    }
                }

                Promise.all(get_promises).then((values) => {
                    console.log("OBTAINED ALL THE VALUES");
                    console.log(values);
                    Promise.all(rename_promises).then((rename_values) => {
                        console.log(rename_values)
                        sftp.end();
                    }).catch((e) => {
                        throw e;
                    })
                })
            })
            .catch(err => {
                sftp.end();
                console.log(err, 'catch error');
            });
    }
}
