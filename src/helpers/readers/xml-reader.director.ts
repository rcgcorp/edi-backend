import { Injectable } from "@nestjs/common";
import { PurchaseOrder } from "src/entities/purchase-order.entity";
import { PurchaseOrdersService } from "src/modules/boms/purchase-orders/purchase-orders.service";
import { LogItemsService } from "src/modules/boms/log-items/log-items.service";
import { SystemService } from "src/modules/system/system.service";
import { ASNReader } from "./asn-reader.service";
import { INVReader } from "./inv-reader.service";
import { PORReader } from "./por-reader.service";
import { IsNull } from "typeorm"
// import { PurchaseOrderBuilder } from "./purchase-order-builder.service";

@Injectable()
export class XMLReaderDirector {

    constructor(
        private PORReader: PORReader,
        private ASNReader: ASNReader,
        private INVReader: INVReader,
        private LogItemsService: LogItemsService,
        private SystemService: SystemService,
        private PurchaseOrdersService: PurchaseOrdersService
    ) {

    }

    async readPOR(xml: any, file: string): Promise<any> {
        try {
            let details = await this.PORReader.getPODetails(xml, file);
            let process_end = await this.checkForASN(details);
            let cancelledorder = await this.checkForCancelled(details);
            if (details && !process_end && !cancelledorder) {
                await this.PORReader.createLogHistory();
            }
            await this.cleanFiles(file, process_end ? 'duplicates' : null);
        } catch (e) {
            await this.saveErrorLog(xml, file, e);
            await this.cleanFiles(file, 'error');
        }
    }

    async readASN(xml: any, file: string): Promise<any> {
        try {
            let asn_processed = false
            let details = await this.ASNReader.getPODetails(xml, file);
            let process_end = await this.checkForCompleteASN(details);
            if (details && !process_end) {
                asn_processed = await this.ASNReader.createLogHistory();
            }

            if (asn_processed) {
                this.PORReader.updateErplyStatus(details, 5);
                await this.cleanFiles(file, process_end ? 'duplicates' : null);
            }
        } catch (e) {
            await this.saveErrorLog(xml, file, e);
            await this.cleanFiles(file, 'error');
        }
    }

    async readINV(xml: any, file: string): Promise<any> {
        try {
            let details = await this.INVReader.getPODetails(xml, file);
            // await this.INVReader.createLogHistory();
            // await this.cleanFiles(file);

            let process_end = await this.checkForINV(details, xml);
            if (details && !process_end) {
                await this.INVReader.createLogHistory();
            }
            await this.cleanFiles(file, process_end ? 'duplicates' : null);
        } catch (e) {
            await this.saveErrorLog(xml, file, e);
            await this.cleanFiles(file, 'error');
        }
    }

    async cleanFiles(file: string, alt_directory?: string): Promise<any> {
        let fs = require('fs'), convert = require('xml-js');
        let directory = './temp/pending/'
        let archive_directory = './temp/archive/'
        let to_directory = `./temp/${alt_directory}/`
        fs.rename(directory + file, (alt_directory ? to_directory : archive_directory) + ((alt_directory=='duplicates') ? Math.floor(Date.now() / 1000) +'_'+ file : file), function (err) {
            if (err) throw err
        })
    }

    async checkForASN(po: PurchaseOrder) {
        let log = await this.LogItemsService.find({
            where: {
                purchase_order_id: po.id,
                type: "despatch_advice"
            },
        })

        if (log) {
            console.log("Found a complete ASN")
            return true;
        } else {
            return false;
        }
    }

    async checkForCompleteASN(po: PurchaseOrder) {
        let log = await this.LogItemsService.find({
            where: {
                purchase_order_id: po.id,
                split_or_complete: "COMPLETE"
            },
        })

        if (log) {
            console.log("Found a complete ASN")
            return true;
        } else {
            return false;
        }
    }

    async checkForCancelled(po: PurchaseOrder) {
        let log = await this.PurchaseOrdersService.find({
            where: {
                id: po.id,
                status: "cancelled"
            },
        })
        let log1 = await this.PurchaseOrdersService.find({
            where: {
                id: po.id,
                status: "rejected"
            },
        })

        if (log || log1) {
            console.log("Found a order already cancelled")
            return true;
        } else {
            return false;
        }
    }

    async checkForINV(po: PurchaseOrder, xml: any) {
        let key = Object.keys(xml)[1];
        let log = await this.LogItemsService.find({
            where: {
                purchase_order_id: po.id,
                type: 'despatch_advice',
                reference_id: xml[key].Header.DespatchAdviceNumber._text,
                reference_log_id: IsNull(),
            }
        })

        if (log) {
            return false;
        } else {
            return true;
        }
    }

    async saveErrorLog(xml: any, file: string, message: any) {
        let key = Object.keys(xml)[1];

        if (typeof message === 'object') {
            message = message.toString()
        }

        await this.SystemService.createError({
            message: message,
            purchase_order: xml[key].Header.PurchaseOrderNumber._text,
            type: key,
            reference_number:  xml[key].Header[key + 'Number']._text,
            file_name: file,
        })
    }
}