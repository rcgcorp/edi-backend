import { Injectable } from "@nestjs/common";
import { ErplyApiService } from "src/api/erply-api/erply-api.service";
import { LogItem } from "src/entities/log-item.entity";
import { PurchaseOrder } from "src/entities/purchase-order.entity";
import { LineItemsService } from "src/modules/boms/line-items/line-items.service";
import { LogItemsService } from "src/modules/boms/log-items/log-items.service";
import { PurchaseOrdersService } from "src/modules/boms/purchase-orders/purchase-orders.service";
import { Connection, IsNull } from "typeorm";
import { XMLReader } from "./xml-reader.interface";
import { PackConversion } from 'src/entities/pack-conversion.entity';
import { getConnection, getRepository, In,Repository} from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class INVReader implements XMLReader {
    file: string;
    xml: any;
    po_details: PurchaseOrder;
    status: string;
    conversion_items_data: any;

    constructor(
        @InjectRepository(PackConversion)
        private PackConversionRepository: Repository<PackConversion>,
        private connection: Connection,
        private PurchaseOrdersService: PurchaseOrdersService,
        private LogItemsService: LogItemsService,
        private LineItemsService: LineItemsService) {
    }

    async getPODetails(data, file): Promise<any> {
        this.file = file;
        this.xml = data;
        let key = Object.keys(data)[1];
        let po_details = await this.PurchaseOrdersService.find({
            where: {
                erply_po_id: data[key].Header.PurchaseOrderNumber._text
            },
            relations: ['supplier']
        });

        if (!po_details)
            throw `Purchase Order ${data[key].Header.PurchaseOrderNumber._text} not found in TEDI`

        this.po_details = po_details;
        return po_details

    }


    /**
     * Get ASN with reference ot invoice
     */
    async createLogHistory() {
        let data = this.xml
        let po_details = this.po_details;
        let key = Object.keys(data)[1];
        let type = this.camelToSnakeCase(key);

        if (data[key].Header.DespatchAdviceNumber._text == null || data[key].Header.DespatchAdviceNumber._text == '')
            throw `Invoice for ${data[key].Header.PurchaseOrderNumber._text} is missing Despatch Advice number`

        let asn = await this.LogItemsService.find({
            where: {
                purchase_order_id: po_details.id,
                type: 'despatch_advice',
                reference_id: data[key].Header.DespatchAdviceNumber._text
            },
        });

        let porsp = await this.LogItemsService.find({
            where: {
                purchase_order_id: po_details.id,
                type: 'purchase_order_response',
            },
            order: {
                created_at: "DESC"
            },
            relations: ['line_items']
        });

        if (!porsp)
            throw `Purchase Order Response not found for ${data[key].Header.PurchaseOrderNumber._text} not found in TEDI and trying to import an Invoice`


        if (!asn)
            throw `Despatch advice ${data[key].Header.DespatchAdviceNumber._text} for ${data[key].Header.PurchaseOrderNumber._text} not found in TEDI and trying to import an Invoice`

        if (asn.reference_log_id == null) {

             //Pack conversion starts    
        let checkAp21Gtin = [];
        if(Object.prototype.toString.call(data.Invoice.LineItems.LineItem) === '[object Object]'){
            checkAp21Gtin.push(data.Invoice.LineItems.LineItem.GTIN._text);
        }else{
            await data.Invoice.LineItems.LineItem.forEach(async (product,index) => {
                checkAp21Gtin.push(data.Invoice.LineItems.LineItem[index].GTIN._text);
            })
        }
        this.conversion_items_data = await this.PackConversionRepository.find({
            where: {
                ap21_gtin: In(checkAp21Gtin)
            }
        })

        if(Object.prototype.toString.call(data.Invoice.LineItems.LineItem) === '[object Object]'){
            let conversion_items = this.conversion_items_data.find(item => item.ap21_gtin == data.Invoice.LineItems.LineItem.GTIN._text)
            if(conversion_items){
                let product_to_compare_price = porsp.line_items.find(line => line.gtin == conversion_items.erply_gtin)
                data.Invoice.LineItems.LineItem.GTIN._text = conversion_items.erply_gtin;
                data.Invoice.LineItems.LineItem.InvoiceQty._text = (data.Invoice.LineItems.LineItem.InvoiceQty._text * conversion_items.ap21_pack_qty); 
                data.Invoice.LineItems.LineItem.UnitPrice._text = product_to_compare_price.unit_price;
                data.Invoice.LineItems.LineItem.LineAmountExcGST._text = (conversion_items.erply_gtin *(data.Invoice.LineItems.LineItem.InvoiceQty._text * conversion_items.ap21_pack_qty));
            }
        }else{
            await data.Invoice.LineItems.LineItem.forEach(async (product,index) => {
                let conversion_items = this.conversion_items_data.find(item => item.ap21_gtin == data.Invoice.LineItems.LineItem[index].GTIN._text);
                if(conversion_items){
                    let product_to_compare_price = porsp.line_items.find(line => line.gtin == conversion_items.erply_gtin)
                    data.Invoice.LineItems.LineItem[index].GTIN._text = conversion_items.erply_gtin;
                    data.Invoice.LineItems.LineItem[index].GTIN._text = conversion_items.erply_gtin;
                    data.Invoice.LineItems.LineItem[index].InvoiceQty._text= (data.Invoice.LineItems.LineItem[index].InvoiceQty._text * conversion_items.ap21_pack_qty); 
                    data.Invoice.LineItems.LineItem[index].UnitPrice._text = product_to_compare_price.unit_price;
                    data.Invoice.LineItems.LineItem[index].LineAmountExcGST._text = (conversion_items.erply_gtin *(data.Invoice.LineItems.LineItem[index].InvoiceQty._text * conversion_items.ap21_pack_qty));                }
            })
        }
        //Pack conversion Ends  

            //Get the Buyer, shipto and supplier codes
            let {buyer, shipTo, supplier} = await this.readNameAddressParty(data[key].Header.NameAddressParty)

            let header_issues = await this.validateHeaderData(buyer, shipTo, supplier, po_details);

            this.status = 'invoice_received';

            //Check if value is in sync with previous log or falls within threshold, otherwise, reject
            let log_item = await this.LogItemsService.create({
                purchase_order_id: po_details.id,
                file_name: this.file,
                message_function: data[key].Header.MessageFunctionCode ? data[key].Header.MessageFunctionCode._text : null,
                reference_id: data[key].Header.InvoiceNumber._text + '_' + data[key].Header.DespatchAdviceNumber._text,
                notes: data[key].Header.Notes ? data[key].Header.Notes._text : null,
                status: this.status,
                type: type,
                total_lines: data[key].Summary.NumberOfLines._text,
                total_amount: data[key].Summary.InvoiceAmountExcGST._text,
                total_amount_gst: data[key].Summary.InvoiceAmountIncGST._text,
                date: data[key].Header.InvoiceDate._text,
                fails_header_threshold: (header_issues.buyer || header_issues.shipTo || header_issues.supplier),
                log_buyer_code: header_issues.buyer ? buyer : null,
                log_ship_to_code: header_issues.shipTo ? shipTo : null,
                log_supplier_code: header_issues.supplier ? supplier : null,

            })

            if (data.Invoice.LineItems.LineItem.constructor == Array) {
                data.Invoice.LineItems.LineItem.forEach(async line_item => {
                    this.insertLineItemData(line_item, log_item, porsp)
                })
            } else {
                this.insertLineItemData(data.Invoice.LineItems.LineItem, log_item, porsp);
            }

            await this.LogItemsService.update(asn, {
                reference_log_id: log_item.id
            });

            await this.PurchaseOrdersService.update(po_details, {
                status: this.status,
            })

            setTimeout(async () => { await this.validateLineItems(po_details, log_item, porsp) }, 30000);
        }


    }

    ///////HELPER FUNCTIONS/////
    /**
    * @param po 
    * @param new_log 
    * @returns 
    * Notes: Steps to complete
    * 1) Check if there is an invoice for each ASN
    * 2) If complete, check the total line qty and price against the last PORSP
    */
    async validateLineItems(po: PurchaseOrder, log: any, porsp: LogItem) {
        let incomplete = await this.LogItemsService.find({
            where: {
                purchase_order_id: po.id,
                type: 'despatch_advice',
                reference_log_id: IsNull()
            },
        });

        if (!incomplete) {
            const invoice_counts = await this.connection.query(`
                SELECT li.gtin as gtin, SUM(li.delivered_qty) as qty, li.unit_price as unit_price FROM line_item li
                LEFT JOIN log_item lo ON li.log_id = lo.id
                WHERE lo.type = 'invoice' AND lo.purchase_order_id = ${po.id}
                GROUP BY li.gtin, li.unit_price
            `);

            porsp.line_items.forEach(async line => {
                let invoice_data = invoice_counts.find(invoice => invoice.gtin == line.gtin);
                if (invoice_data) {
                    if (parseInt(invoice_data.qty) != line.delivered_qty) {
                        await this.connection.query(`
                    UPDATE line_item li
                    RIGHT JOIN log_item lo ON lo.id = li.log_id
                    SET li.fails_quantity_threshold = 1
                    WHERE li.gtin = '${invoice_data.gtin}'
                    AND lo.purchase_order_id = ${po.id}
                    AND lo.type = 'invoice'
                    `);
                    }

                    if (parseFloat(invoice_data.unit_price) != line.unit_price) {
                        await this.connection.query(`
                    UPDATE line_item li
                    RIGHT JOIN log_item lo ON lo.id = li.log_id
                    SET li.fails_price_threshold = 1
                    WHERE li.gtin = '${invoice_data.gtin}'
                    AND lo.purchase_order_id = ${po.id}
                    AND lo.type = 'invoice'
                    `);
                    }
                }
                else {
                    let missing_item = line;
                    missing_item.log_id = log.id;
                    missing_item.fails_missing_item = true;
                    missing_item.delivered_qty = 0;
                    await this.LineItemsService.create(missing_item)
                }
            });

            invoice_counts.forEach(async invoice => {
                let porsp_data = porsp.line_items.find(line => invoice.gtin == line.gtin);
                if (!porsp_data) {
                    await this.connection.query(`
                    UPDATE line_item li
                    RIGHT JOIN log_item lo ON lo.id = li.log_id
                    SET li.fails_unknown_item = 1
                    WHERE li.gtin = '${invoice.gtin}'
                    AND lo.purchase_order_id = ${po.id}
                    AND lo.type = 'despatch_advice'
                `);
                }
            })

            // porsp.line_items.forEach(async line => {
            //     let invoice_data = invoice_counts.find(invoice => invoice.gtin == line.gtin);
            //     if (parseFloat(invoice_data.unit_price) != line.unit_price) {
            //         await this.connection.query(`
            //         UPDATE line_item li
            //         RIGHT JOIN log_item lo ON lo.id = li.log_id
            //         SET li.fails_price_threshold = 1
            //         WHERE li.gtin = '${invoice_data.gtin}'
            //         AND lo.purchase_order_id = ${po.id}
            //         AND lo.type = 'invoice'
            //         `);
            //     }
            // });

        }
    }

    async validateHeaderData(buyer, shipTo, supplier, po: PurchaseOrder) {
        //Compare the date differences
        let issues = {
            buyer: false,
            shipTo: false,
            supplier: false,
        };

        if (buyer != process.env.TAF_BUYER_ID) {
            issues.buyer = true
        }
        if (shipTo != po.warehouse_id) {
            issues.shipTo = true
        }
        if (supplier != po.supplier.erply_supplier_id) {
            issues.supplier = true
        }

        console.log('Any header issues?', issues);

        return issues;
    }

    async insertLineItemData(product, log_item, porsp: LogItem) {
        let details = porsp.line_items.find(line => line.gtin == product.GTIN._text)
        // Look for product details from existing line items.
        let ex = await this.LineItemsService.find({
            gtin: product.GTIN._text,
        })
        let product_id = ex ? ex.product_id : null;
        let style_code = ex ? ex.style_code : null;
        let size_code = ex ? ex.size_code : null;

        await this.LineItemsService.create({
            log_id: log_item.id,
            gtin: product.GTIN._text,
            product_id: details ? details.product_id : product_id,
            style_code: details ? details.style_code : style_code,
            description: product.ProductDescription._text,
            colour_code: null,
            order_qty: details ? details.delivered_qty : 0,
            delivered_qty: product.InvoiceQty._text,
            unit_price: details ? details.unit_price : 0,
            delivered_unit_price: product.UnitPrice._text,
            tax_rate: parseFloat(product.UnitPrice._text) * 0.1,
            size_code: details ? details.size_code : size_code,
            fails_unknown_item: details ? false : true
        })
    }

    async readNameAddressParty(xmlData: any) {
        try {
            let buyer = null;
            buyer = xmlData.find(data => data.PartyCodeQualifier._text == 'BUYER').PartyIdentifier._text
            let shipTo = null;
            shipTo = xmlData.find(data => data.PartyCodeQualifier._text == 'SHIPTO').PartyIdentifier._text
            let supplier = null;
            supplier = xmlData.find(data => data.PartyCodeQualifier._text == 'SUPPLIER').PartyIdentifier._text

            return {
                buyer,
                shipTo,
                supplier
            };
        } catch (e) {
            throw `Error when reading NameAddressParty fields`
        }
    }

    getStatus({ qty_issues, price_issues }) {
        if (qty_issues.length + price_issues.length > 0)
            return 'requires_action'
        else
            return 'invoice_received'
    }

    camelToSnakeCase(str: string) {
        return str.replace(/[A-Z]/g, (letter, index) => { return index == 0 ? letter.toLowerCase() : '_' + letter.toLowerCase(); });
    }
}