import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApiModule } from 'src/api/api.module';
import { ErplyApiService } from 'src/api/erply-api/erply-api.service';
import { LogError } from 'src/entities/log-error.entity';
import { PurchaseOrdersModule } from 'src/modules/boms/purchase-orders/purchase-orders.module';
import { SuppliersModule } from 'src/modules/boms/suppliers/suppliers.module';
import { ErplyModule } from 'src/modules/erply/erply.module';
import { SystemModule } from 'src/modules/system/system.module';
import { MailModule } from '../mail/mail.module';
import { ASNReader } from './asn-reader.service';
import { INVReader } from './inv-reader.service';
import { PORReader } from './por-reader.service';
import { PackConversion } from 'src/entities/pack-conversion.entity';
// import { PurchaseOrderBuilder } from './purchase-order-builder.service';
import { XMLReaderDirector } from './xml-reader.director';

@Module({
    imports: [TypeOrmModule.forFeature([PackConversion]),SystemModule, PurchaseOrdersModule, SuppliersModule, MailModule, ErplyModule, ApiModule],
    providers: [PORReader, ASNReader, INVReader, XMLReaderDirector],
    exports: [PORReader, ASNReader, INVReader, XMLReaderDirector]
})
export class XmlReaderModule {}
