import { Injectable } from "@nestjs/common";
import { ErplyApiService } from "src/api/erply-api/erply-api.service";
import { LogItem } from "src/entities/log-item.entity";
import { PurchaseOrder } from "src/entities/purchase-order.entity";
import { LineItemsService } from "src/modules/boms/line-items/line-items.service";
import { LogItemsService } from "src/modules/boms/log-items/log-items.service";
import { PurchaseOrdersService } from "src/modules/boms/purchase-orders/purchase-orders.service";
import { XMLReader } from "./xml-reader.interface";
import * as moment from 'moment-timezone'
import { LineItem } from "src/entities/line-item.entity";
import { Connection } from "typeorm";
import { PackConversion } from 'src/entities/pack-conversion.entity';
import { getConnection, getRepository, In,Repository} from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class ASNReader implements XMLReader {
    file: string;
    xml: any;
    po_details: PurchaseOrder;
    status: string;
    conversion_items_data: any;

    constructor(
        @InjectRepository(PackConversion)
        private PackConversionRepository: Repository<PackConversion>,
        private connection: Connection,
        private PurchaseOrdersService: PurchaseOrdersService,
        private LogItemsService: LogItemsService,
        private LineItemsService: LineItemsService) {
    }

    async getPODetails(data, file): Promise<any> {
        this.file = file;
        this.xml = data;
        let key = Object.keys(data)[1];
        let po_details = await this.PurchaseOrdersService.find({
            where: {
                erply_po_id: data[key].Header.PurchaseOrderNumber._text
            },
            relations: ['supplier']
        });

        if (!po_details)
            throw `Purchase Order ${data[key].Header.PurchaseOrderNumber._text} not found in TEDI`

        this.po_details = po_details;
        return po_details

    }

    async createLogHistory() {
        let data = this.xml
        let po_details = this.po_details;
        let key = Object.keys(data)[1];
        let type = this.camelToSnakeCase(key); 

        let porsp = await this.LogItemsService.find({
            where: {
                type: 'purchase_order_response',
                purchase_order_id: po_details.id
            },
            order: {
                created_at: "DESC"
            },
            relations: ['line_items']
        })

        if (!porsp)
            throw `Purchase Order Response not found for ${data[key].Header.PurchaseOrderNumber._text} not found in TEDI and trying to import a Despatch Advice`

        if (po_details.status == 'requires_action') { //Still pending on buyer's action, skip this ASN.
            return false
        }

        if (porsp.message_function != 'ACCEPTED' && po_details.status == 'requires_action') {
            porsp = await this.LogItemsService.find({
                where: {
                    type: 'purchase_order',
                    purchase_order_id: po_details.id,
                    message_function: 'REPLACE'
                },
                order: {
                    created_at: "DESC"
                },
                relations: ['line_items']
            })
        }

        //Check to see if the PO already has ASN log history marked as COMPLETE - if so, don't consume it and just move the file to archive.


        if (data[key].Header.SplitOrCompleteShipment._text == 'COMPLETE') {
            this.status = 'complete_asn_received'
        } else {
            this.status = 'split_asn_received'
        }
        
        //Get the Buyer, shipto and supplier codes
        let {buyer, shipTo, supplier} = await this.readNameAddressParty(data[key].Header.NameAddressParty)

        let header_issues = await this.validateHeaderData(buyer, shipTo, supplier, po_details);
        let date_issues = await this.validateDates(data[key].Header, porsp);
        let shipmentTrackNumber = (data[key].Header.ShipmentTrackingNumber)?data[key].Header.ShipmentTrackingNumber._text:data[key].Header.ShipmentTrackingURL._text
        //Check if value is in sync with previous log or falls within threshold, otherwise, reject
        let log_item = await this.LogItemsService.create({
            purchase_order_id: po_details.id,
            message_function: data[key].Header.SplitOrCompleteShipment ? data[key].Header.SplitOrCompleteShipment._text : null,
            file_name: this.file,
            reference_id: data[key].Header.DespatchAdviceNumber._text,
            status: this.status,
            type: type,
            date: data[key].Header.DespatchAdviceDate._text,
            ship_date: data[key].Header.ShipDate._text,
            total_lines: data[key].Header.ShipmentCartonCount._text,
            estimated_delivery_date: data[key].Header.EstimatedDeliveryDate._text,
            split_or_complete: data[key].Header.SplitOrCompleteShipment._text,
            consignment_note_number: data[key].Header.ConsignmentNoteNumber._text,
            carrier_name: data[key].Header.CarrierName._text,
            shipment_tracking_number: shipmentTrackNumber,
            fails_date_threshold: date_issues,
            fails_header_threshold: (header_issues.buyer || header_issues.shipTo || header_issues.supplier),
            log_buyer_code: header_issues.buyer ? buyer : null,
            log_ship_to_code: header_issues.shipTo ? shipTo : null,
            log_supplier_code: header_issues.supplier ? supplier : null,
        })

        
        let line_items = this.combinePackageLines(data[key].Packages.Package);
        let line_items_array = [];

        //Pack conversion starts    
        this.conversion_items_data = await this.PackConversionRepository.find({
            where: {
                ap21_gtin: In(Object.keys(line_items))
            }
        })

        if(this.conversion_items_data.length){
            for (const line_item in line_items) {
                let conversion_items = this.conversion_items_data.find(item => item.ap21_gtin == line_item);
                if(conversion_items){
                    line_items[conversion_items.erply_gtin] = line_items[line_item]
                    line_items[conversion_items.erply_gtin].ShipQty =  (line_items[line_item].ShipQty * conversion_items.ap21_pack_qty); 
                    delete line_items[line_item];
                }
            }
        }
        //Pack conversion Ends 

        
        for (const line_item in line_items) {
            let product = porsp.line_items.find(line => line.gtin == line_item)
            let lineItemParams = {
                log_id: log_item.id,
                gtin: line_item,
                product_id: product ? product.product_id : null,
                style_code: product ? product.style_code : null,
                description: product ? product.description : null,
                unit_price: product ? product.unit_price : null,
                delivered_unit_price: product ? product.unit_price : null,
                tax_rate: product ? product.tax_rate : null,
                size_code: product ? product.size_code : null,
                colour_code: product ? product.colour_code : null,
                order_qty: product ? product.delivered_qty : null,
                delivered_qty: line_items[line_item].ShipQty,
                sscc: line_items[line_item].SSCC,
            }
            if (!product) {
                let ex = await this.LineItemsService.find({
                    gtin: line_item,
                })
                if (ex) {
                    lineItemParams.product_id = ex.product_id;
                    lineItemParams.style_code = ex.style_code;
                    lineItemParams.size_code = ex.size_code;
                    lineItemParams.description = ex.description;
                }
            }
            let li = await this.LineItemsService.create(lineItemParams)

            line_items_array.push(li);
        }

        let total_price = this.calculateTotal(line_items_array);
        await this.LogItemsService.update(log_item, {
            total_amount: total_price,
            total_amount_gst: parseFloat((total_price * 1.1).toFixed(2)),
        })

        //Validate the total numbers here

        await this.PurchaseOrdersService.update(po_details, {
            status: this.status,
        })

        if (log_item.message_function == 'COMPLETE')
            setTimeout(async () => { await this.validateLineItems(po_details, log_item, porsp) }, 30000);

        return true
    }

    ///////HELPER FUNCTIONS/////

    /**
     * 
     * @param po 
     * @param new_log 
     * @returns 
     * Notes: Steps to complete
     * 1) Grab the threshhold values from the PO supplier and grab the most recent purchase_order type log_item
     * 2) Compare the line items from said log_item to see if there are any issues with threshold
     * 3) IF there are append their product_id to array
     */
    async validateLineItems(po: PurchaseOrder, log: any, porsp: LogItem) {
        const asn_counts = await this.connection.query(`
        SELECT li.gtin as gtin, SUM(li.delivered_qty) as qty, li.unit_price as unit_price FROM line_item li
        LEFT JOIN log_item lo ON li.log_id = lo.id
        WHERE lo.type = 'despatch_advice' AND lo.purchase_order_id = ${po.id}
        GROUP BY li.gtin, li.unit_price
    `);

        porsp.line_items.forEach(async line => {
            let asn_data = asn_counts.find(asn => asn.gtin == line.gtin);
            if (asn_data) {
                if (parseInt(asn_data.qty) != line.delivered_qty) {
                    await this.connection.query(`
                        UPDATE line_item li
                        RIGHT JOIN log_item lo ON lo.id = li.log_id
                        SET li.fails_quantity_threshold = 1
                        WHERE li.gtin = '${asn_data.gtin}'
                        AND lo.purchase_order_id = ${po.id}
                        AND lo.type = 'despatch_advice'
                    `);
                }
            } else {
                let missing_item = line;
                missing_item.log_id = log.id;
                missing_item.fails_missing_item = true;
                missing_item.delivered_qty = 0;
                await this.LineItemsService.create(missing_item)
            }
        });

        asn_counts.forEach(async asn => {
            let porsp_data = porsp.line_items.find(line => asn.gtin == line.gtin);
            if (!porsp_data) {
                await this.connection.query(`
                    UPDATE line_item li
                    RIGHT JOIN log_item lo ON lo.id = li.log_id
                    SET li.fails_unknown_item = 1
                    WHERE li.gtin = '${asn.gtin}'
                    AND lo.purchase_order_id = ${po.id}
                    AND lo.type = 'despatch_advice'
                `);
            }
        })

    }

    async validateDates(header_data: any, last_po_log: LogItem) {
        //Compare the date differences
        let issue = false;
        if (moment(last_po_log.delivery_earliest_date.toJSON()).isAfter(header_data.EstimatedDeliveryDate._text, 'day') ||
            moment(last_po_log.delivery_latest_date.toJSON()).isBefore(header_data.EstimatedDeliveryDate._text, 'day')
        ) {
            issue = true;
        }

        return issue;
    }

    async validateHeaderData(buyer, shipTo, supplier, po: PurchaseOrder) {
        //Compare the date differences
        let issues = {
            buyer: false,
            shipTo: false,
            supplier: false,
        };

        if (buyer != process.env.TAF_BUYER_ID) {
            issues.buyer = true
        }
        if (shipTo != po.warehouse_id) {
            issues.shipTo = true
        }
        if (supplier != po.supplier.erply_supplier_id) {
            issues.supplier = true
        }

        console.log('Any header issues?', issues);

        return issues;
    }

    async readNameAddressParty(xmlData: any) {
        try {
            let buyer = null;
            buyer = xmlData.find(data => data.PartyCodeQualifier._text == 'BUYER').PartyIdentifier._text
            let shipTo = null;
            shipTo = xmlData.find(data => data.PartyCodeQualifier._text == 'SHIPTO').PartyIdentifier._text
            let supplier = null;
            supplier = xmlData.find(data => data.PartyCodeQualifier._text == 'SUPPLIER').PartyIdentifier._text

            return {
                buyer,
                shipTo,
                supplier
            };
        } catch (e) {
            throw `Error when reading NameAddressParty fields`
        }
    }

    combinePackageLines(packages) {
        let line_items = {}
        if (packages.constructor != Array) {
            packages = [packages];
        }
        packages.forEach(pack => {
            console.log("Pack is ", pack);
            if (pack.LineItems.LineItem.constructor == Array) {
                pack.LineItems.LineItem.forEach(line => {
                    if (line.GTIN._text in line_items) {
                        line_items[line.GTIN._text].ShipQty += parseInt(line.ShipQty._text);
                        line_items[line.GTIN._text].SSCC += ',' + line.ShipQty._text;
                    }
                    else {
                        line_items[line.GTIN._text] = {
                            ShipQty: parseInt(line.ShipQty._text),
                            SSCC: pack.SSCC._text
                        };
                    }
                });
            } else {
                if (pack.LineItems.LineItem.GTIN._text in line_items) {
                    line_items[pack.LineItems.LineItem.GTIN._text].ShipQty += parseInt(pack.LineItems.LineItem.ShipQty._text);
                    line_items[pack.LineItems.LineItem.GTIN._text].SSCC += ',' + pack.SSCC._text
                }
                else {
                    line_items[pack.LineItems.LineItem.GTIN._text] = {
                        ShipQty: parseInt(pack.LineItems.LineItem.ShipQty._text),
                        SSCC: pack.SSCC._text
                    };
                }
            }
        });

        return line_items;
    }

    calculateTotal(line_items: Array<LineItem>): number {
        return parseFloat(line_items.map(line_item => {
            return (line_item.unit_price * line_item.delivered_qty).toFixed(2)
        }).reduce((previousValue, currentValue) => (parseFloat(previousValue) + parseFloat(currentValue)).toFixed(2)))
    }

    getStatus({ qty_issues, price_issues }) {
        if (qty_issues.length + price_issues.length > 0)
            return 'requires_action'
        else
            return 'complete_asn_received'
    }

    camelToSnakeCase(str: string) {
        return str.replace(/[A-Z]/g, (letter, index) => { return index == 0 ? letter.toLowerCase() : '_' + letter.toLowerCase(); });
    }
}