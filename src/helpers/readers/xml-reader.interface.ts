import { PurchaseOrder } from "src/entities/purchase-order.entity";

export interface XMLReader {
    file: string
    xml: object
    po_details: PurchaseOrder
    status: string
    /**
     * Note: Gets the OG purchase order based on the PO number
     */
    getPODetails(data: any, file: string): any
    /**
     * Creates the log and line items for the document received from SPS
     * @return XMLObject
     */
    createLogHistory(): any

}