import { Injectable } from "@nestjs/common";
import { ErplyApiService } from "src/api/erply-api/erply-api.service";
import { LogItem } from "src/entities/log-item.entity";
import { PurchaseOrder } from "src/entities/purchase-order.entity";
import { LineItemsService } from "src/modules/boms/line-items/line-items.service";
import { LogItemsService } from "src/modules/boms/log-items/log-items.service";
import { PurchaseOrdersService } from "src/modules/boms/purchase-orders/purchase-orders.service";
import { SuppliersSerrvice } from "src/modules/boms/suppliers/suppliers.service";
import { ErplyPurchaseOrderService } from "src/modules/erply/erply-purchase-order/erply-purchase-order.service";
import { UsingJoinColumnOnlyOnOneSideAllowedError } from "typeorm";
import { MailService } from "../mail/mail.service";
import { XMLReader } from "./xml-reader.interface";
import * as moment from 'moment-timezone'
import { Exception } from "handlebars";
import { PackConversion } from 'src/entities/pack-conversion.entity';
import { Connection, getConnection, getRepository, In,Repository} from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class PORReader implements XMLReader {
    file: string;
    xml: any;
    po_details: PurchaseOrder;
    status: string;
    conversion_items_data: any;

    constructor(
        @InjectRepository(PackConversion)
        private PackConversionRepository: Repository<PackConversion>,
        private ErplyPuchaseOrderService: ErplyPurchaseOrderService,
        private ErplyApiService: ErplyApiService,
        private PurchaseOrdersService: PurchaseOrdersService,
        private LogItemsService: LogItemsService,
        private LineItemsService: LineItemsService,
        private SuppliersSerrvice: SuppliersSerrvice,
        private MailService: MailService) {
    }

    async getPODetails(data, file): Promise<any> {
        this.file = file;
        this.xml = data;
        let key = Object.keys(data)[1];
        let po_details = await this.PurchaseOrdersService.find({
            where: {
                erply_po_id: data[key].Header.PurchaseOrderNumber._text
            },
            relations: ['supplier']
        });

        if (!po_details)
            throw `Purchase Order ${data[key].Header.PurchaseOrderNumber._text} not found in TEDI`

        this.po_details = po_details;
        return po_details

    }

    async createLogHistory() {
        let data = this.xml
        let po_details = this.po_details;
        let key = Object.keys(data)[1];
        let type = this.camelToSnakeCase(key);

        let last_po_log = await this.LogItemsService.find({
            where: {
                type: 'purchase_order',
                purchase_order_id: po_details.id
            },
            order: {
                created_at: "DESC"
            },
            relations: ['line_items']
        });

        if (!last_po_log)
            throw `Original Purchase Order not found for ${data[key].Header.PurchaseOrderNumber._text} not found in TEDI and trying to import a Purchase Order Response`

        //Pack conversion starts    
        let checkAp21Gtin = [];
        if(Object.prototype.toString.call(data.PurchaseOrderResponse.LineItems.LineItem) === '[object Object]'){
            checkAp21Gtin.push(data.PurchaseOrderResponse.LineItems.LineItem.GTIN._text);
        }else{
            await data.PurchaseOrderResponse.LineItems.LineItem.forEach(async (product,index) => {
                checkAp21Gtin.push(data.PurchaseOrderResponse.LineItems.LineItem[index].GTIN._text);
            })
        }
        this.conversion_items_data = await this.PackConversionRepository.find({
            where: {
                ap21_gtin: In(checkAp21Gtin)
            }
        })

        if(Object.prototype.toString.call(data.PurchaseOrderResponse.LineItems.LineItem) === '[object Object]'){
            let conversion_items = this.conversion_items_data.find(item => item.ap21_gtin == data.PurchaseOrderResponse.LineItems.LineItem.GTIN._text)
            if(conversion_items){
                let product_to_compare_price = last_po_log.line_items.find(item => item.gtin == conversion_items.erply_gtin);
                data.PurchaseOrderResponse.LineItems.LineItem.GTIN._text = conversion_items.erply_gtin;
                data.PurchaseOrderResponse.LineItems.LineItem.OrderQty._text =  (data.PurchaseOrderResponse.LineItems.LineItem.OrderQty._text * conversion_items.ap21_pack_qty); 
                data.PurchaseOrderResponse.LineItems.LineItem.DeliverQty._text =  (data.PurchaseOrderResponse.LineItems.LineItem.DeliverQty._text * conversion_items.ap21_pack_qty); 
                data.PurchaseOrderResponse.LineItems.LineItem.UnitPrice._text =  product_to_compare_price.unit_price;
            }
        }else{
            await data.PurchaseOrderResponse.LineItems.LineItem.forEach(async (product,index) => {
                let conversion_items = this.conversion_items_data.find(item => item.ap21_gtin == data.PurchaseOrderResponse.LineItems.LineItem[index].GTIN._text);
                if(conversion_items){
                    let product_to_compare_price = last_po_log.line_items.find(item => item.gtin == conversion_items.erply_gtin);
                    data.PurchaseOrderResponse.LineItems.LineItem[index].GTIN._text = conversion_items.erply_gtin;
                    data.PurchaseOrderResponse.LineItems.LineItem[index].OrderQty._text =  (data.PurchaseOrderResponse.LineItems.LineItem[index].OrderQty._text * conversion_items.ap21_pack_qty); 
                    data.PurchaseOrderResponse.LineItems.LineItem[index].DeliverQty._text =  (data.PurchaseOrderResponse.LineItems.LineItem[index].DeliverQty._text * conversion_items.ap21_pack_qty); 
                    data.PurchaseOrderResponse.LineItems.LineItem[index].UnitPrice._text =  product_to_compare_price.unit_price;
                }
            })

        }
        //Pack conversion Ends  
        
        //Get the Buyer, shipto and supplier codes
        let {buyer, shipTo, supplier} = await this.readNameAddressParty(data[key].Header.NameAddressParty)

        let header_issues = await this.validateHeaderData(buyer, shipTo, supplier, po_details);
        let { qty_issues, price_issues, backorder_issues, unknown_issues, missing_items, price_changed, qty_changed,qty_issues_within,price_issues_within,qty_increase,qty_decrease } = await this.validateLineItems(po_details, data.PurchaseOrderResponse, last_po_log);
        let { date_issues, date_changed } = await this.validateDates(data[key].Header, last_po_log);
        this.status = await this.getStatus({ qty_issues, price_issues, backorder_issues, unknown_issues, missing_items, date_issues: date_issues, price_changed, qty_changed,qty_increase,qty_decrease }, po_details, data[key].Header.MessageFunctionCode ? data[key].Header.MessageFunctionCode._text : null);

        if (data.PurchaseOrderResponse.LineItems.LineItem.constructor != Array) {
            data.PurchaseOrderResponse.LineItems.LineItem = [data.PurchaseOrderResponse.LineItems.LineItem]
        }

        let total_price = data.PurchaseOrderResponse.LineItems.LineItem.map(line_item => {
            return (parseFloat(line_item.UnitPrice._text) * parseFloat(line_item.DeliverQty._text)).toFixed(2)
        }).reduce((previousValue, currentValue) => parseFloat(previousValue) + parseFloat(currentValue))


        //Check if value is in sync with previous log or falls within threshold, otherwise, reject
        let log_item = await this.LogItemsService.create({
            purchase_order_id: po_details.id,
            message_function: data[key].Header.MessageFunctionCode ? data[key].Header.MessageFunctionCode._text : null,
            file_name: this.file,
            reference_id: data[key].Header.PurchaseOrderResponseNumber._text,
            notes: data[key].Header.Notes ? data[key].Header.Notes._text : null,
            contract_number: data[key].Header.ContractNumber ? data[key].Header.ContractNumber._text : null,
            status: this.status,
            type: type,
            total_lines: data[key].Summary.NumberOfLines._text,
            total_amount: total_price,
            total_amount_gst: parseFloat((total_price * 1.1).toFixed(2)),
            date: data[key].Header.PurchaseOrderResponseDate._text,
            delivery_earliest_date: data[key].Header.DeliveryDateEarliest._text,
            delivery_latest_date: data[key].Header.DeliveryDateLatest._text,
            fails_date_threshold: date_issues,
            fails_header_threshold: (header_issues.buyer || header_issues.shipTo || header_issues.supplier),
            log_buyer_code: header_issues.buyer ? buyer : null,
            log_ship_to_code: header_issues.shipTo ? shipTo : null,
            log_supplier_code: header_issues.supplier ? supplier : null,

        })

        data.PurchaseOrderResponse.LineItems.LineItem.forEach(async line_item => {
            console.log("==============================");
            this.insertLineItemData(last_po_log, line_item, log_item, qty_issues, price_issues, unknown_issues, po_details, qty_changed,qty_issues_within,price_issues_within,qty_decrease)
        })

        /**
         * Insert missing data
         */
        missing_items.forEach(async missing_item => {
            missing_item.log_id = log_item.id;
            missing_item.fails_missing_item = true;
            missing_item.delivered_qty = 0;
            await this.LineItemsService.create(missing_item)
        })


        await this.PurchaseOrdersService.update(this.po_details, {
            contract_number: data[key].Header.ContractNumber ? data[key].Header.ContractNumber._text : null,
            status: this.status,
        })

        //Handle Message Funcion changes
        if (data[key].Header.ContractNumber) {
            await this.ErplyPuchaseOrderService.appendContractNumber(this.po_details.erply_base_id, data[key].Header.ContractNumber._text).toPromise()
        }

        //if (this.status == 'change_accepted')
        //If the status has been "CHANGED" make sure to do something about it - an auto replacement will be sent in another scheduled task.

        if (this.status == 'response_received') {
            await this.updateErplyStatus(po_details, 5);
        }

        if (this.status == 'rejected' || this.status == 'cancelled') {
            log_item.purchase_order = this.po_details;
            await this.LogItemsService.rejectEvent(log_item);
        }

    }

    ///////HELPER FUNCTIONS/////

    /**
     * 
     * @param po 
     * @param new_log 
     * @returns 
     * Notes: Steps to complete
     * 1) Grab the threshhold values from the PO supplier and grab the most recent purchase_order type log_item
     * 2) Compare the line items from said log_item to see if there are any issues with threshold
     * 3) IF there are append their product_id to array
     */
    async validateLineItems(po: PurchaseOrder, log: any, last_po_log: LogItem) {
        let qty_issues = [];
        let qty_issues_within = [];
        let price_issues = [];
        let price_issues_within = [];
        let backorder_issues = [];
        let missing_items = [];
        let unknown_issues = [];
        let price_changed = false;
        let qty_changed = [];
        let qty_increase = false;
        let qty_decrease= false;
        let quantity_variation = po.supplier.quantity_variation;
        let price_variation = po.supplier.price_variation;

        let line_items = last_po_log.line_items;

        if (log.LineItems.LineItem.constructor != Array) {
            log.LineItems.LineItem = [log.LineItems.LineItem]
        }

        line_items.forEach(line_item => {
            let product_code = line_item.gtin;
            if (!log.LineItems.LineItem.find(product => product.GTIN._text == product_code)) {
                missing_items.push(line_item);
            }
        });

        log.LineItems.LineItem.forEach(async product => {
            let product_code = product.GTIN._text;
            let product_to_compare = line_items.find(item => item.gtin == product_code);

            if (product_to_compare) {
                let acceptable_price = product_to_compare.unit_price * ((100 + price_variation) / 100);
                if (parseFloat(product.UnitPrice._text) > acceptable_price) {
                    //Price unacceptable
                    price_issues.push(product_code)
                }else if (parseFloat(product.UnitPrice._text) != product_to_compare.unit_price) {
                    // && (parseFloat(product.UnitPrice._text) < acceptable_price)
                    price_issues_within.push(product_code+'_within')
                }

                if (parseFloat(product.UnitPrice._text) != product_to_compare.unit_price && price_changed == false) {
                    //Price changed
                    price_changed = true;
                }

                //Check Quantity Variation -> Check if there are less products
                let acceptable_qty = product_to_compare.order_qty * ((100 - quantity_variation) / 100);
                if ((parseInt(product.DeliverQty._text) < acceptable_qty && !(po.type == 'RELEASE' && po.erply_blanket_id == null))
                || parseInt(product.DeliverQty._text) > product_to_compare.order_qty) { // Refill PO qty decrease is okay.
                    //Quantity unacceptable
                    qty_issues.push(product_code)
                }else if ( parseInt(product.DeliverQty._text) != product_to_compare.order_qty && !(po.type == 'RELEASE' && po.erply_blanket_id == null)) { // Refill PO qty decrease is okay.
                    //((parseInt(product.DeliverQty._text) < acceptable_qty && !(po.type == 'RELEASE' && po.erply_blanket_id == null)) || parseInt(product.DeliverQty._text) > product_to_compare.order_qty)
                    //Quantity unacceptable
                    qty_issues_within.push(product_code+'_within')
                }

                if (parseInt(product.DeliverQty._text) != product_to_compare.order_qty) {
                    //Quantity changed
                    qty_changed.push(product_code)
                    if(parseInt(product.DeliverQty._text)>product_to_compare.order_qty && !qty_increase){
                        qty_increase = true;
                    }else if(parseInt(product.DeliverQty._text)<product_to_compare.order_qty && !qty_decrease){
                        qty_decrease = true;
                    }

                }

                if (parseInt(product.DeliverQty._text) != product_to_compare.order_qty) {
                    //Quantity changed
                    qty_changed.push(product_code)
                }

                if (product.BackOrderQty && parseInt(product.BackOrderQty._text) > 0) {
                    //Quantity unacceptable
                    backorder_issues.push(product_code)
                }
            } else {
                unknown_issues.push(product_code)
            }
        });

        return {
            qty_issues,
            price_issues,
            backorder_issues,
            unknown_issues,
            missing_items,
            price_changed,
            qty_changed,
            qty_issues_within,
            price_issues_within,
            qty_increase,
            qty_decrease
        };
    }

    async validateDates(header_data: any, last_po_log: LogItem) {
        //Compare the date differences
        let date_issues = false;
        let date_changed = false;

        if (moment(last_po_log.delivery_earliest_date.toJSON()).isAfter(header_data.DeliveryDateEarliest._text, 'day') ||
            moment(last_po_log.delivery_latest_date.toJSON()).isBefore(header_data.DeliveryDateLatest._text, 'day')
        ) {
            date_issues = true;
        }

        if (!moment(last_po_log.delivery_earliest_date.toJSON()).isSame(header_data.DeliveryDateEarliest._text, 'day') ||
        !moment(last_po_log.delivery_latest_date.toJSON()).isSame(header_data.DeliveryDateLatest._text, 'day')
        ) {
            date_changed = true;
        }

        return { date_issues, date_changed };
    }

    async validateHeaderData(buyer, shipTo, supplier, po: PurchaseOrder) {
        //Compare the date differences
        let issues = {
            buyer: false,
            shipTo: false,
            supplier: false,
        };

        if (buyer != process.env.TAF_BUYER_ID) {
            issues.buyer = true
        }
        if (shipTo != po.warehouse_id && shipTo != 'Z99') {
            issues.shipTo = true
        }
        if (supplier != po.supplier.erply_supplier_id) {
            issues.supplier = true
        }

        console.log('Any header issues?', issues);

        return issues;
    }

    async insertLineItemData(last_po_log, product, log_item, qty_issues, price_issues, unknown_issues, po: PurchaseOrder, qty_changed,qty_issues_within,price_issues_within,qty_decrease) {
        let product_code = product.GTIN._text;
        let original_product = last_po_log.line_items.find(item => item.gtin == product_code)
        let qty_issue = qty_issues.find(product => product == product_code) ? true : false;
        let qty_issue_within = qty_issues_within.find(product => product ==  product_code+'_within') ? true : false;
        let price_issue = price_issues.find(product => product == product_code) ? true : false;
        let price_issue_within = price_issues_within.find(product => product ==   product_code+'_within') ? true : false;
        let unknown_issue = unknown_issues.find(product => product == product_code) ? true : false;

        if (po.type == 'RELEASE' && po.erply_blanket_id == null && qty_changed.length > 0 && qty_issue == false && qty_decrease) {
            // any qty decrease in refill should be accpted but also escalated to buyer.
            qty_issue = qty_changed.find(product => product == product_code) ? true : false;
        }

        let lineItemParams = {
            log_id: log_item.id,
            gtin: product.GTIN ? product.GTIN._text : null,
            product_id: original_product ? original_product.product_id : null,
            style_code: original_product ? original_product.style_code : null,
            size_code: original_product ? original_product.size_code : null,
            description: product.ProductDescription._text,
            colour_code: null,
            order_qty: original_product ? original_product.order_qty : 0,
            delivered_qty: product.DeliverQty._text,
            backordered_qty: product.BackOrderQty ? product.BackOrderQty._text : 0,
            backorder_date: product.BackOrderDate ? product.BackOrderDate._text : null,
            unit_price: original_product ? original_product.unit_price : 0,
            delivered_unit_price: product.UnitPrice._text,
            fails_quantity_threshold: qty_issue,
            fails_price_threshold: price_issue,
            fails_unknown_item: unknown_issue,
            fails_quantity_within_threshold: qty_issue_within,
            fails_price_within_threshold: price_issue_within,
            tax_rate: parseFloat(product.UnitPrice._text) * 0.1,
        }

        if (unknown_issue) {
            let ex = await this.LineItemsService.find({
                gtin: product.GTIN._text,
            })
            if (ex) {
                lineItemParams.product_id = ex.product_id;
                lineItemParams.style_code = ex.style_code;
                lineItemParams.size_code = ex.size_code;
            }
        }

       await this.LineItemsService.create(lineItemParams)
        // console.log('line item created');
    }

    async readNameAddressParty(xmlData: any) {
        try {
            let buyer = null;
            buyer = xmlData.find(data => data.PartyCodeQualifier._text == 'BUYER').PartyIdentifier._text
            let shipTo = null;
            shipTo = xmlData.find(data => data.PartyCodeQualifier._text == 'SHIPTO').PartyIdentifier._text
            let supplier = null;
            supplier = xmlData.find(data => data.PartyCodeQualifier._text == 'SUPPLIER').PartyIdentifier._text

            return {
                buyer,
                shipTo,
                supplier
            };
        } catch (e) {
            throw `Error when reading NameAddressParty fields`
        }
    }

    /***
     * STEPS: Update the PO with new stuff
     * Get all line items of this PO
     * Loop through them
     * Send an update request to ERPLY to update all the line items.
     * If within threshold, automatically update the PO with new details and change edi_status to 3
     */
    async updateErplyStatus(po: PurchaseOrder, status: number) {
        return this.ErplyApiService.sendRequest({
            id: po.erply_base_id,
            attributeName1: 'edi_status',
            attributeType1: 'int',
            attributeValue1: status,
        }, 'savePurchaseDocument').toPromise();
    }

    async getStatus({ qty_issues, price_issues, backorder_issues, unknown_issues, missing_items, date_issues, price_changed, qty_changed,qty_increase,qty_decrease },  po: PurchaseOrder, status?: string) {
        if (status == 'REJECTED') {
            return 'rejected'
        }

        if (date_issues && po.type == 'RELEASE' && po.erply_blanket_id == null) { // Only cancel refill orders when there is date issue.
            return 'cancelled'
        }

        if (qty_issues.length + price_issues.length + backorder_issues.length + unknown_issues.length + missing_items.length > 0 ||
            (po.type == 'RELEASE' && po.erply_blanket_id == null && qty_changed.length > 0 && qty_increase)) { //If qty changed for refill order now, could only be decrease.
            return 'requires_action'
        }

        if (price_changed || qty_changed.length > 0 || date_issues) {
            return 'change_accepted'
        }
        return 'response_received'
    }

    camelToSnakeCase(str: string) {
        return str.replace(/[A-Z]/g, (letter, index) => { return index == 0 ? letter.toLowerCase() : '_' + letter.toLowerCase(); });
    }
}