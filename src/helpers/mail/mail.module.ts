import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { Module } from '@nestjs/common';
import { MailService } from './mail.service';
import { join } from 'path';
import { SystemModule } from 'src/modules/system/system.module';

@Module({
  imports: [
    MailerModule.forRoot({
      // transport: 'smtps://user@example.com:topsecret@smtp.example.com',
      // or
      transport: {
        host: 'smtprelay.accentgr',
        port: 25,
        secure: false,
        tls: { rejectUnauthorized: false }
      },
      defaults: {
        from: '"No Reply" <taf-edi@accentgr.com.au>',
      },
      template: {
        dir: join(__dirname, 'templates'),
        adapter: new HandlebarsAdapter(), // or new PugAdapter() or new EjsAdapter()
        options: {
          strict: false,
        },
      },
    }),
    SystemModule
  ],
  providers: [MailService],
  exports: [MailService], 
})
export class MailModule {}