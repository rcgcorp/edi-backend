import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { SystemService } from 'src/modules/system/system.service';

@Injectable()
export class MailService {
    constructor(private mailerService: MailerService, private systemService: SystemService) { }

    async sendErrorEmail(recipients: any, subject: string, purchase_order: string, error: string) {
        this.mailerService.sendMail({
            to: recipients,
            // from: '"Support Team" <support@example.com>', // override default from
            subject: subject,
            template: './error', // `.hbs` extension is appended automatically
            context: { // ✏️ filling curly brackets with content
                purchase_order: purchase_order,
                error: error
            },
        });
    }

    async sendEscalationEmail(payload: any) {
        let {qty_issues, header_issues, price_issues, date_issues, style_issues, rejected_issues} = await this.classifyEscalationIssues(payload)

        let recipients = await this.systemService.findEmails({ // System Recipients.
            where: {
                type: 'buyers'
            }
        });

        let supplier_ids =  Array.from(new Set(payload.map(p => p.supplier_id)))
        let  system_recipients_emails =  Array.from(new Set(recipients.map(p => p.email.toLowerCase())))
        for (let supplier_id of supplier_ids) {
            await this.sendBuyerEscalationEmailForSupplierRecipients(supplier_id, system_recipients_emails, payload)
        }

        await this.sendBuyerEscalationEmail(recipients, qty_issues, date_issues, price_issues, rejected_issues, header_issues, style_issues)
    }

    async sendBuyerEscalationEmailForSupplierRecipients(supplier_id, system_recipients, payload) {
        let supplier_email_recipients = await this.systemService.findSupplierEmailRecipients({supplier_id: supplier_id})
        let recipients_should_send = supplier_email_recipients.filter(recipient => !system_recipients.includes(recipient.email.toLowerCase())) // Exclude system recipients.

        if (recipients_should_send.length > 0) {
            let supplier_payload = payload.filter(data => data.supplier_id == supplier_id);
            let {qty_issues, header_issues, price_issues, date_issues, style_issues, rejected_issues} = await this.classifyEscalationIssues(supplier_payload)
            await this.sendBuyerEscalationEmail(recipients_should_send, qty_issues, date_issues, price_issues, rejected_issues, header_issues, style_issues)
        }
    }

    async classifyEscalationIssues(payload) {
        let qty_issues = payload.filter(data => data.fails_quantity_threshold == true && data.rejected != 'REJECTED');
        let header_issues = payload.filter(data => data.fails_header_threshold == true).filter((log, index, self) =>
            index === self.findIndex((l) => (
                l.reference_id === log.reference_id
            ))
        );
        
        let price_issues = payload.filter(data => data.fails_price_threshold == true);
        let date_issues = payload.filter(data => data.fails_date_threshold == true);
        let style_issues = payload.filter(data => data.fails_unknown_item == true || data.fails_missing_item == true);
        let rejected_issues = payload.filter(data => data.rejected == 'REJECTED');

        return {
            qty_issues, header_issues, price_issues, date_issues, style_issues, rejected_issues
        }
    }

    async sendBuyerEscalationEmail(recipients, qty_issues, date_issues, price_issues, rejected_issues, header_issues, style_issues) {
        return  this.mailerService.sendMail({
            to: recipients.map(email => email.email),
            // from: '"Support Team" <support@example.com>', // override default from
            subject: "[EDI] PO Escalations",
            template: './escalation', // `.hbs` extension is appended automatically
            context: { // ✏️ filling curly brackets with content
                qty_issues: qty_issues,
                has_qty_issues: qty_issues.length ? true : false,
                date_issues: date_issues,
                has_date_issues: date_issues.length ? true : false,
                price_issues: price_issues,
                has_price_issues: price_issues.length ? true : false,
                rejected_issues: rejected_issues,
                has_rejected_issues: rejected_issues.length ? true : false,
                has_header_issues: header_issues.length ? true : false,
                header_issues: header_issues,
                has_style_issues: style_issues.length ? true : false,
                style_issues: style_issues,

            },
        });
    }

    async sendErrorEscalationEmail(payload: any) {

        let recipients = await this.systemService.findEmails({
            where: {
                type: 'errors'
            }
        });

        this.mailerService.sendMail({
            to: recipients.map(email => email.email),
            // from: '"Support Team" <support@example.com>', // override default from
            subject: "[EDI] TEDI Import Errors ",
            template: './errors', // `.hbs` extension is appended automatically
            context: { // ✏️ filling curly brackets with content
                errors: payload

            },
        });
    }

    async sendBuyerNotificationEmail(payload: any) {

        let recipients = await this.systemService.findEmails({
            where: {
                type: 'buyers'
            }
        });

        this.mailerService.sendMail({
            to: recipients.map(email => email.email),
            subject: "[EDI] TEDI Invalid Cancellation",
            template: './buyer-invalid-cancellation', // `.hbs` extension is appended automatically
            context: { // ✏️ filling curly brackets with content
                errors: payload

            },
        });
    }

    async sendSystemStateErrorEmail(errorLogs: any) {
        if (errorLogs.length > 0) {
            let recipients = process.env.MAIL_ERROR_RECIPIENTS.split(",");
            this.mailerService.sendMail({
                to: recipients,
                subject: process.env.APP_URL+" [EDI] TEDI system pos sync error logs",
                template: './system-po-sync-error', // `.hbs` extension is appended automatically
                context: { // ✏️ filling curly brackets with content
                    error_logs: errorLogs
                },
            });
        }
    }

    async sendUnlockSystemStateEmail() {
        let recipients = process.env.MAIL_ERROR_RECIPIENTS.split(",");
        this.mailerService.sendMail({
            to: recipients,
            subject: process.env.APP_URL+ " [EDI] TEDI system pos sync is locked",
            template: './system-locked-state', // `.hbs` extension is appended automatically
            context: {
            },
        });
    }
}