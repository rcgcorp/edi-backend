import { Injectable } from "@nestjs/common";
import { PackConversion } from 'src/entities/pack-conversion.entity';
import { Connection, getConnection, getRepository, In} from 'typeorm';
import { ErplyApiService } from "src/api/erply-api/erply-api.service";
import { XMLBuilder } from "./xml-builder.interface";
import * as xml from "object-to-xml";
import { vatrates } from 'src/helpers/constants/vatrates';
import { XmlBuilderDirector } from 'src/helpers/builders/xml/xml-builder.director';
 
@Injectable()
export class PurchaseOrderBuilder implements XMLBuilder {

   
    xml: object;
    type: string;


    constructor(  
        
    ) {
       
        this.type = "PurchaseOrder"
        this.xml = {};

    }


    /**
     * 
     * @param po 
     * @param supplier_address 
     * @param warehouse_address 
     * @return void
     */
    addHeader(po: any, supplier_address: any, warehouse_address: any, log: any): void {
        const buyer = {
            address: process.env.TAF_BUYER_ADDRESS,
            city: process.env.TAF_BUYER_CITY,
            state: process.env.TAF_BUYER_STATE,
            postcode: process.env.TAF_BUYER_POSTCODE,
            country: process.env.TAF_BUYER_COUNTRY,
            phone: process.env.TAF_BUYER_PHONE,
            email: process.env.TAF_BUYER_EMAIL,
        }

        //The buyer code adjusts wther the PO is a blanket or a release
        let shipto_code = po.type != 'RELEASE' ? process.env.BLANKET_BUYER_ID : po.warehouse_id;

        this.xml['Header'] = {
            PurchaseOrderNumber: po.erply_po_id,
            MessageFunctionCode: log.message_function,
            PurchaseOrderType: po.type == 'BLANKET' ? 'INDENT' : 'RELEASE',
            PurchaseOrderDate: log.date,
            DeliveryDateEarliest: log.delivery_earliest_date,
            DeliveryDateLatest: log.delivery_latest_date,
            ContractNumber: log.contract_number,
            BlanketOrderNumber: po.erply_blanket_id,
            Notes: log.notes,
            NameAddressParty: [
                this.createAddressParty("SUPPLIER", po.supplier.erply_supplier_id, po.supplier.name, supplier_address),
                po.type != 'RELEASE' ? this.createAddressParty("SHIPTO", shipto_code, process.env.TAF_BUYER_PARTY_NAME, buyer) : this.createAddressParty("SHIPTO", shipto_code, po.warehouse_name, warehouse_address),
                this.createAddressParty("BUYER", process.env.TAF_BUYER_ID, process.env.TAF_BUYER_PARTY_NAME, buyer)
            ],
            Currency: 'AUD' //don't let this be statis
        };
    }


    /**
     * 
     * @param order 
     * @return Promise<void>
     */
    async addBody(order: any): Promise<void> {
        let parsed = [];
        let totalExgst = 0;

        for (const [index, row] of order.line_items.sort((a, b) => {
            if (b.style_code && a.style_code && b.size_code && a.size_code) {
                return a.style_code.localeCompare(b.style_code) || parseInt(a.size_code) - parseInt(b.size_code)
            } else {
                return b.id > a.id
            }
            }).entries()) {

            const tax = (row.unit_price * vatrates[row.vatrateID]) / 100;

            var PackConversionRepositary = await getRepository(PackConversion);
            let conversion_items = await PackConversionRepositary.find({
                where: {
                    erply_gtin: row.gtin
                }
            })

            console.log("============test gtn=================");
            console.log(conversion_items);
            console.log("============test gtn=================");
    
            let LineAmountExcGSTTemp = (row.unit_price * row.order_qty)
            if(conversion_items.length > 0 ){
                row.gtin = conversion_items[0].ap21_gtin;    
                row.order_qty =  (row.order_qty/conversion_items[0].ap21_pack_qty); 
                row.unit_price =  conversion_items[0].ap21_cost; 
                LineAmountExcGSTTemp = (row.unit_price * row.order_qty);
            }

            totalExgst = totalExgst + LineAmountExcGSTTemp;
            
            parsed.push({
                LineItem: {
                    LineNumber: index + 1,
                    GTIN: row.gtin,
                    ProductStyleCode: row.style_code,
                    ProductDescription: row.description,
                    // ProductColourCode: row.colour_code,
                    ProductSizeCode: row.size_code,
                    OrderQty: row.order_qty,
                    UnitPrice: row.unit_price.toFixed(2),
                    TaxRate: process.env.TAF_TAX_RATE, //row.tax_rate,
                    LineAmountExcGST: LineAmountExcGSTTemp.toFixed(2),
                }
            })
        }
        const summary = {
            NumberOfLines: order.total_lines,
            OrderAmountExcGST: totalExgst.toFixed(2),
            OrderAmountGST: (totalExgst/10).toFixed(2)
        }
        this.xml = {
            ...this.xml, ...{
                LineItems: parsed,
                Summary: summary
            }
        };
    }


    /**
     * 
     * @returns XMLObject
     */
    generateXML(): any {
        let object = {};
        object[this.type] = this.xml;
        return xml(object);
    }


    /**
     * @return void
     */
    reset(): void {
        this.xml = {};
    }


    /**
     * 
     * @param qualifier 
     * @param id 
     * @param name 
     * @param address 
     * @param email 
     * @param phone 
     * @returns object
     */
    private createAddressParty(qualifier: string, id: any, name: string, address: any): object {
        return {
            PartyCodeQualifier: qualifier,
            PartyIdentifier: id,
            PartyName: name,
            Address1: address.address,
            Address2: address.address2,
            City: address.city,
            State: address.state,
            Postcode: address.postcode,
            Country: "AU",
            Phone: address.phone,
            Email: address.email,
        }
    }
}