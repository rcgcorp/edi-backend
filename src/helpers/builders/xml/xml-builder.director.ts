import { Injectable } from "@nestjs/common";
import { PurchaseOrderBuilder } from "./purchase-order-builder.service";

@Injectable()
export class XmlBuilderDirector {
    po: any;
    supplier_address: any;
    warehouse_address: any;
    log: any;

    constructor() {

    }

    init(po: any, supplier_address: any, warehouse_address: any, log: any) {
        this.po = po;
        this.supplier_address = supplier_address;
        this.warehouse_address = warehouse_address;
        this.log = log;
    }

    async buildPurchaseOrderXML(po: any, supplier_address: any, warehouse_address: any, log: any): Promise<any> {
        let builder = new PurchaseOrderBuilder()
        builder.reset();
        builder.addHeader(po,
            supplier_address,
            warehouse_address,
            log
        );
        await builder.addBody(log);
        return builder.generateXML();
    }
}