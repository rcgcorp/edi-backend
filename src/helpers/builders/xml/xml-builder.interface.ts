export interface XMLBuilder {
    xml: object
    type: string
    /**
     * Note: Creates the EDI XML header that contains PO details and address parties
     * @param po 
     * @param supplier_address 
     * @param warehouse_address 
     */
    addHeader(po: any, supplier_address: any, warehouse_address: any, log: any): void
    /**
     * Note: Creates element tags that will be added to existing xml elements
     * @param data 
     */
    addBody(data: object): void
    /**
     * Generates the XML schema based on object created
     * @return XMLObject
     */
    generateXML(): any
    /**
     * resets xml data in class
     * @return null
     */
    reset(): void
}