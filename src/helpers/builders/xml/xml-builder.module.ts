import { Module } from '@nestjs/common';
import { ApiModule } from 'src/api/api.module';
import { PurchaseOrderBuilder } from './purchase-order-builder.service';
import { XmlBuilderDirector } from './xml-builder.director';

@Module({
    imports: [ApiModule],
    providers: [PurchaseOrderBuilder, XmlBuilderDirector],
    exports: [PurchaseOrderBuilder, XmlBuilderDirector]
})
export class XmlBuilderModule {}
