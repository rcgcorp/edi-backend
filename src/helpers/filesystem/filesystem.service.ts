import { Injectable } from '@nestjs/common';
import { promises } from "fs";
const {  readdir, stat } = promises;
import * as moment from 'moment-timezone'

@Injectable()
export class FilesystemService {
    async getFiles(directory?: string) {
        let d = directory ? `./temp/${directory}/` : './temp/'
        let files = await readdir(d, {
            withFileTypes: false
        });

        return await Promise.all(files.map(async file => {
            let item = await stat(d + file);
            console.log({
                file_name: file,
                type: item.isDirectory ? 'd' : 'f',
                ...item
            });
            return {
                file_name: file,
                type: item.isDirectory() ? 'd' : 'f',
                date: moment(item.ctime).tz('Australia/Sydney').format('DD MMM YYYY HH:mm:ss'),
                ...item
            }
        }))
    }

    async downloadFile(directory: string, filename: string) {
        let file_path = `./temp/${directory == 'null' ? filename : directory + '/' + filename}`
        let fs = require('fs');
        let content = fs.readFileSync(file_path, "utf8");
        return content;
    }
}
