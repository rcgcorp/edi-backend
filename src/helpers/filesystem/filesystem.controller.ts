import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { JwtGuard } from 'src/modules/auth/jwt.guard';
import { FilesystemService } from './filesystem.service';

@Controller('filesystem')
export class FilesystemController {
    constructor(private FileSystemService:FilesystemService) {}

    @Get()
    // @UseGuards(JwtGuard)
    async getFiles(@Param() params) {
        let data = await this.FileSystemService.getFiles();
        return data;
    }

    @Get(':directory')
    // @UseGuards(JwtGuard)
    async getDirectoryFiles(@Param() params) {
        return await this.FileSystemService.getFiles(params.directory);
    }

    @Get(':directory/:filename')
    // @UseGuards(JwtGuard)
    async downloadFile(@Param() params) {
        let xml = await this.FileSystemService.downloadFile(params.directory, params.filename);
        return xml;
    }
}
