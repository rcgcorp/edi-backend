import { HttpModule, Module } from '@nestjs/common';
import { ErplyApiService } from './erply-api/erply-api.service';

@Module({
    imports: [
        HttpModule,
    ],
    providers: [
        ErplyApiService
    ],
    exports: [
        ErplyApiService
    ]
})
export class ApiModule { }
