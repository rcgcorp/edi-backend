const qs = require('querystring')

export class ErplyRequestObject {
    private readonly endpoint: string
    private readonly username: string
    private readonly secret: string
    private readonly clientCode: number
    request: string;
    sessionKey: string;
    params: object;


    constructor(client_code: number) {
        this.endpoint = process.env.ERPLY_API_ENDPOINT;
        this.username = process.env.ERPLY_API_USER;
        this.secret = process.env.ERPLY_API_SECRET;
        this.clientCode = client_code ? client_code : parseInt(process.env.ERPLY_API_CLIENTCODE);
    }

    setRequest(request: string) {
        this.request = request;
    }

    setParams(params: object) {
        this.params = params;
    }

    setSessionKey(sessionkey: string) {
        this.sessionKey = sessionkey;
    }

    getErplyEndpoint(): string {
        return this.endpoint;
    }

    getRequestParams(): string {
        return qs.stringify({
            'clientCode': this.clientCode,
            'username': this.username,
            'password': this.secret,
            'request': this.request,
            'sessionKey': this.sessionKey,
            'sendContentType': 1, //Default value. Unsure what this does
            ...this.params
        });
    }

    getBulkRequestParams(requests: any): string {
        return qs.stringify({
            'clientCode': this.clientCode,
            'requests': JSON.stringify(requests),
            'sessionKey': this.sessionKey,
            'sendContentType': 1, //Default value. Unsure what this does
            responseType: "json",
            ...this.params
        });
    }
}
