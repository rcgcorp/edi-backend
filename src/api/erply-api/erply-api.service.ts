import { Injectable, HttpService, Logger } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { ErplyRequestObject } from './request-object.dto';

@Injectable()
export class ErplyApiService {

    /**
        * ERPLY API workflow
        * 1) Sends an HTTP request to ERPLY api (verifyRequest)
        * 2) Receives session key and append sto requestObject
        * 3) updates request parameter to suit original ERPLY API Request
        */

    constructor(private httpService: HttpService) {
    }

    init(client_code?: number): Observable<Object> {
        const requestObject: ErplyRequestObject = new ErplyRequestObject(client_code);
        requestObject.setRequest('verifyUser');
        return this.httpService.post(
            requestObject.getErplyEndpoint(),
            requestObject.getRequestParams()
        );
    }


    sendRequest(params: object, request?: string, client_code?: number): Observable<Object> {
        let obj = null;
        const requestObject: ErplyRequestObject = new ErplyRequestObject(client_code);
        // if (request)
        requestObject.setRequest(request);
        return this.init(client_code).pipe(
            mergeMap(response => {
                // console.log(response['data'])
                try {
                    obj = response['data']
                    // console.log("Session information", response['data']['requests'], response['data'], response['data']['records'])
                    requestObject.setSessionKey(response['data']['records'][0]['sessionKey']);
                    requestObject.setRequest(request);
                    requestObject.setParams(params);
                    return this.httpService.post(
                        requestObject.getErplyEndpoint(),
                        requestObject.getRequestParams()
                    ).pipe(map(response => response['data']));
                } catch (e) {
                    console.log("Object casuing the error... ", obj)
                    throw e;
                }
            })
        );
    }

    sendRequestWithSessionKey(params: object, sessionKey: string, request?: string, client_code?: number): Observable<Object> {
        let obj = null;
        const requestObject: ErplyRequestObject = new ErplyRequestObject(client_code);
        try {
            // console.log("Session information", response['data']['requests'], response['data'], response['data']['records'])
            requestObject.setSessionKey(sessionKey);
            requestObject.setRequest(request);
            requestObject.setParams(params);
            return this.httpService.post(
                requestObject.getErplyEndpoint(),
                requestObject.getRequestParams()
            ).pipe(map(response => response['data']));
        } catch (e) {
            console.log("Object casuing the error... ", obj)
            throw e;
        }
    }

    sendBulkRequestWithSessionKey(sessionKey: string, requests: any, client_code?: number) {
        const requestObject: ErplyRequestObject = new ErplyRequestObject(client_code);
        try {
            requestObject.setSessionKey(sessionKey);
            return this.httpService.post(
                requestObject.getErplyEndpoint(),
                requestObject.getBulkRequestParams(requests)
            ).pipe(map(response => response['data']));
        } catch (e) {
            console.log(requests)
            console.log("Object casuing the error... ")
            throw e;
        }
    }
}
