import { Controller, Get, Response } from '@nestjs/common';
import { ErplyApiService } from 'src/api/erply-api/erply-api.service';
import { PurchaseOrdersService } from '../boms/purchase-orders/purchase-orders.service';
import { PurchaseOrderBuilder } from 'src/helpers/builders/xml/purchase-order-builder.service';
import { XmlBuilderDirector } from 'src/helpers/builders/xml/xml-builder.director';
import * as fs from 'fs';
import { vatrates } from 'src/helpers/constants/vatrates';
import { ErplyPurchaseOrderService } from '../erply/erply-purchase-order/erply-purchase-order.service';
import { ErplySupplierService } from '../erply/erply-supplier/erply-supplier.service';
import { ErplySuppliersService } from '../boms/erply-suppliers/erply-suppliers.service';
import { ErplyAddressService } from '../erply/erply-address/erply-address.service';
import { SuppliersSerrvice } from '../boms/suppliers/suppliers.service';
import { Connection, UsingJoinColumnIsNotAllowedError } from 'typeorm';
import { LogItemsService } from '../boms/log-items/log-items.service';
import { LineItemsService } from '../boms/line-items/line-items.service';
import { SftpService } from 'src/helpers/sftp/sftp.service';
import { ImportFilesService } from 'src/schedules/sps/import-files/import-files.service';
import { GetPurchaseOrdersService } from 'src/schedules/erply/get-purchase-orders/get-purchase-orders.service';
import { TransferFilesService } from 'src/schedules/sftp/transfer-files/transfer-files.service';
import { throws } from 'assert';
import { MailService } from 'src/helpers/mail/mail.service';
import * as moment from 'moment-timezone';

@Controller('sandbox')
export class SandboxController {
    constructor(
        private connection: Connection,
        private SuppliersSerrvice: SuppliersSerrvice,
        private SftpService: SftpService,
        private MailService: MailService,
        private LineItemsService: LineItemsService,
        private LogItemsService: LogItemsService,
        private ErplyApiService: ErplyApiService,
        private ErplyPuchaseOrderService: ErplyPurchaseOrderService
    ) { }

    @Get()
    async sandbox(@Response() res) {
        // this.ErplyPurchaseOrderService.index(5000000328).subscribe(async response => {
        //     const po: any = response['records'][0];

        //     res.send(await this.PurchaseOrdersService.create({
        //         erply_po_id: po.id,
        //         supplier_id: po.supplierID,
        //         warehouse_id: po.warehouseID,
        //         type: po.type,
        //     }));
        // });
        // await this.ErplyPuchaseOrderService.appendContractNumber(33000010183, "CON12345").toPromise()
        // return "Done"

        // let recipients = await this.SuppliersSerrvice.getRecipients(17956648);
        // console.log("Recipients are ", recipients);


        // let logItems = await this.connection.query(`
        //     SELECT po.erply_po_id, MAX(lo.reference_id) AS reference_id, lo.type, CONCAT(li.style_code, CONCAT('-', li.size_code)) AS style_code, li.order_qty, 
        //     SUM( li.delivered_qty ) AS delivered_qty, li.unit_price, MAX(li.delivered_unit_price) AS delivered_unit_price, li.fails_quantity_threshold, li.fails_price_threshold,  MAX(lo.fails_date_threshold) AS fails_date_threshold, lo.message_function AS rejected FROM purchase_order po 
        //     LEFT JOIN log_item lo ON po.id = lo.purchase_order_id
        //     LEFT JOIN line_item li ON lo.id = li.log_id
        //     WHERE lo.escalated = FALSE AND 
        //     (lo.fails_date_threshold = TRUE OR li.fails_price_threshold OR li.fails_quantity_threshold OR lo.message_function = 'CANCEL')
        //     GROUP BY po.erply_po_id, lo.type, li.style_code, li.size_code, li.unit_price, li.order_qty ,li.fails_quantity_threshold, li.fails_price_threshold, lo.message_function

        // `);
        // logItems = Object.values(JSON.parse(JSON.stringify(logItems)))
        // if (logItems.length) {
        //     await this.MailService.sendEscalationEmail(Object.values(JSON.parse(JSON.stringify(logItems))))
        // }

        // await this.connection.query(`
        //     UPDATE log_item lo
        //     LEFT JOIN line_item li ON lo.id = li.log_id
        //     SET lo.escalated = TRUE
        //     WHERE lo.escalated = FALSE AND 
        //     (lo.fails_date_threshold = TRUE OR li.fails_price_threshold OR li.fails_quantity_threshold OR lo.message_function = 'REJECTED')
        // `)

        console.log(moment().tz('Australia/Sydney').subtract('00:30:00').format('YYYY-MM-DD HH:mm:ss'))

        // res.send(await this.SuppliersSerrvice.findAll({
        //     relations: ['documents'],
        // }))

        // let line_items = await this.LineItemsService.findAll({
        //     where: {
        //         log_id: 136
        //     }
        // });
        // let requests = JSON.stringify(line_items.map(item => {
        //     return {
        //         requestName: 'getProducts',
        //         recordsOnPage:1,
        //         code: item.style_code + (item.size_code ? `-${item.size_code}` : '')
        //     }
        // }));

        // console.log(requests);
        // let products = await this.ErplyApiService.sendRequest({requests: requests}).toPromise();

        // let update_data = {};
        // let counter = 1;

        // console.log(products);
        // for (const item of line_items) {
        //     let style_code = item.style_code + (item.size_code ? `-${item.size_code}` : '')
        //     // console.log(products['requests'][0]['records'][0].code);
        //     let product = products['requests'].map(request => request.records[0]).find(product => product.code == style_code);
        //     if(!product) {
        //         //THROW SOMETHING HERE TO KILL EVERYTHING
        //         throw "Product " + style_code + " not found in ERPLY"
        //     }

        //     update_data[`productID${counter}`] = product.productID;
        //     update_data[`vatrateID${counter}`] = process.env.TAF_VATRATE_ID;
        //     update_data[`amount${counter}`] = item.delivered_qty;
        //     update_data[`price${counter}`] = item.unit_price;

        //     console.log("Found ypur product", update_data)
        //     counter++;
        // }
        // return update_data;
    }

}
