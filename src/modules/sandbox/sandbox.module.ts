import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApiModule } from 'src/api/api.module';
import { ErplySupplier } from 'src/entities/erply-suppliers.entity';
import { LineItem } from 'src/entities/line-item.entity';
import { LogItem } from 'src/entities/log-item.entity';
import { PurchaseOrder } from 'src/entities/purchase-order.entity';
import { XmlBuilderModule } from 'src/helpers/builders/xml/xml-builder.module';
import { MailModule } from 'src/helpers/mail/mail.module';
import { SftpModule } from 'src/helpers/sftp/sftp.module';
import { SchedulesModule } from 'src/schedules/schedules.module';
import { ErplySuppliersModule } from '../boms/erply-suppliers/erply-suppliers.module';
import { ErplySuppliersService } from '../boms/erply-suppliers/erply-suppliers.service';
import { LineItemsService } from '../boms/line-items/line-items.service';
import { LogItemsService } from '../boms/log-items/log-items.service';
import { PurchaseOrdersModule } from '../boms/purchase-orders/purchase-orders.module';
import { PurchaseOrdersService } from '../boms/purchase-orders/purchase-orders.service';
import { SuppliersModule } from '../boms/suppliers/suppliers.module';
import { ErplyModule } from '../erply/erply.module';
import { SandboxController } from './sandbox.controller';

@Module({
  imports: [ApiModule, ErplyModule, XmlBuilderModule, ErplySuppliersModule, SuppliersModule, PurchaseOrdersModule, SftpModule, MailModule],
  controllers: [SandboxController],
  providers: []
})
export class SandboxModule { }
