import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ErplyApiService } from 'src/api/erply-api/erply-api.service';

@Injectable()
export class ErplyAddressService {
    constructor(
        private ErplyApiService: ErplyApiService,
    ) { }

    /**
     * 
     * @param id Customer ID, supplier ID, or your own company's ID. See getCustomers and getSuppliers. (Customer and supplier IDs do not overlap.)
     * @returns 
     */
    index(id: number): Observable<Object> {
        return this.ErplyApiService.sendRequest({
            supplierID: id,
        }, 'getAddresses');
    }


    bulkIndex(ids: Array<number>): Observable<Object> {
        const requests_array = [];

        for(const id of ids) {
            requests_array.push({
                requestName: 'getAddresses',
                ownerID: id
            });
        }
        return this.ErplyApiService.sendRequest({
            requests: JSON.stringify(requests_array),

        }, null)
    }
}
