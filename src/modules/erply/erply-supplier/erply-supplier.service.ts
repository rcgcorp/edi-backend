import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ErplyApiService } from 'src/api/erply-api/erply-api.service';

@Injectable()
export class ErplySupplierService {
    constructor(
        private ErplyApiService: ErplyApiService,
    ) { }

    index(page: number): Observable<Object> {
        return this.ErplyApiService.sendRequest({
            orderBy: 'name',
            mode: 'SUPPLIERS',
            recordsOnPage: 100,
            pageNo: page
        }, 'getSuppliers');
    }

    bulkIndex(pages: number): Observable<Object> {
        const requests_array = [];

        for(let page = 1; page <= pages; page++) {
            requests_array.push({
                requestName: 'getSuppliers',
                orderBy: 'name',
                mode: 'SUPPLIERS',
                recordsOnPage: 100,
                pageNo: page,
            });
        }
        return this.ErplyApiService.sendRequest({
            requests: JSON.stringify(requests_array),

        }, null)
    }

}
