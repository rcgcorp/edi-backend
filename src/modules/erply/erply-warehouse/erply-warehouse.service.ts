import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ErplyApiService } from 'src/api/erply-api/erply-api.service';

@Injectable()
export class ErplyWarehouseService {
    constructor(
        private ErplyApiService: ErplyApiService,
    ) { }

    index(): Observable<Object> {
        return this.ErplyApiService.sendRequest({
            lang: 'eng',
        }, 'getWarehouses');
    }

    // bulkIndex(pages: number): Observable<Object> {
    //     const requests_array = [];

    //     for(let page = 1; page <= pages; page++) {
    //         requests_array.push({
    //             requestName: 'getWarehouses',
    //             orderBy: 'name',
    //             lang: 'eng',
    //             recordsOnPage: 100,
    //             pageNo: page,
    //         });
    //     }
    //     return this.ErplyApiService.sendRequest({
    //         requests: JSON.stringify(requests_array),

    //     }, null)
    // }

}
