import { Test, TestingModule } from '@nestjs/testing';
import { ErplyConfigService } from './erply-config.service';

describe('ErplyConfigService', () => {
  let service: ErplyConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ErplyConfigService],
    }).compile();

    service = module.get<ErplyConfigService>(ErplyConfigService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
