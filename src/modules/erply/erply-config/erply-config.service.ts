import { HttpService, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ErplyRequestObject } from 'src/api/erply-api/request-object.dto';
const qs = require('querystring')

@Injectable()
export class ErplyConfigService {

    constructor(private httpService: HttpService) {
    }

    verifyUser(client_code: number,
        username: string,
        secret: string,): Observable<object> {
        const requestObject: ErplyRequestObject = new ErplyRequestObject(client_code);
        console.log(requestObject.getErplyEndpoint());
        return this.httpService.post(
            requestObject.getErplyEndpoint(),
            qs.stringify({
                'clientCode': client_code,
                'username': username,
                'password': secret,
                'request': 'verifyUser',
                'sessionLength': 86400
            })
        );
    }
}
