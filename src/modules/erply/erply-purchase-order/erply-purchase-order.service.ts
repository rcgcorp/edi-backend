import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ErplyApiService } from 'src/api/erply-api/erply-api.service';
import * as moment from 'moment-timezone'
@Injectable()
export class ErplyPurchaseOrderService {

    private INTERVAL: number = 15;
    private client_code: number;
    public session_key: '';
    public session_key_exp: number = 0;

    constructor(
        private ErplyApiService: ErplyApiService,
    ) {
        if (process.env.NODE_ENV == 'development') {
            this.client_code = parseInt(process.env.ERPLY_API_CLIENTCODE)
        } else {
            this.client_code = 401;
        }

        this.getSessionKey()
    }

    getSessionKey() {
        if (this.session_key !== '' && this.session_key_exp > moment().tz('Australia/Sydney').unix()) {
            return this.session_key
        }

        let key_now = this.session_key
        this.session_key_exp = this.session_key_exp + 2000;
        const skeyPromise = this.ErplyApiService.init(this.client_code).toPromise().then((response) => {
            this.session_key = response['data']['records'][0]['sessionKey']
            this.session_key_exp = response['data']['status']['requestUnixTime'] + 2000 // session life is 3600.
        }).catch(e => {
            console.log('error occurs when trying to get session token')
            console.log(e)
        })
        return key_now
    }

    async index(supplier_ids: Array<Number>, edi_status: Number) {
        let session_key = this.getSessionKey()
        const records_on_page = 100;
        let pages = 1;
        console.log(Math.floor((Date.now() - (this.INTERVAL * 60 * 1000))));
        let all_promises = []

        let first_page = await this.ErplyApiService.sendRequestWithSessionKey({
            supplierIDs: supplier_ids.join(','),
            searchAttributeName: "edi_status",
            searchAttributeValue: edi_status,
            getRowsForAllInvoices: 1,
            recordsOnPage: records_on_page,
            type: 'PRCORDER',
            orderBy: 'documentID',
            orderByDir: 'asc',
            stateID: 3
            // changedSince: Math.floor((Date.now() - (this.INTERVAL * 60 * 1000)))
        }, session_key, 'getPurchaseDocuments', this.client_code).toPromise();

        if (first_page['records'].length > 0) {
            all_promises = all_promises.concat(first_page)
            if (first_page['status'].errorCode == 0) {
                pages = Math.ceil(first_page['status']['recordsTotal'] / records_on_page);

                if (pages > 1) {
                    for (let page = 2; page < pages + 1; page++) {
                        let page_promise = this.ErplyApiService.sendRequestWithSessionKey({
                            supplierIDs: supplier_ids.join(','),
                            searchAttributeName: "edi_status",
                            searchAttributeValue: edi_status,
                            getRowsForAllInvoices: 1,
                            recordsOnPage: records_on_page,
                            type: 'PRCORDER',
                            orderBy: 'documentID',
                            orderByDir: 'asc',
                            stateID: 3,
                            pageNo: page
                            // changedSince: Math.floor((Date.now() - (this.INTERVAL * 60 * 1000)))
                        }, session_key, 'getPurchaseDocuments', this.client_code).toPromise()
                        all_promises.push(page_promise)
                    }
                }
            }
        }

        return all_promises
    }

    async cancelledIndex(supplier_ids: Array<Number>) {
        let session_key = this.getSessionKey()
        const records_on_page = 100;
        let pages = 1;
        console.log(moment().subtract(1, 'hour').unix());
        const last_hour = moment().subtract(1, 'hour').unix()
        let all_promises = []
        let first_page = await this.ErplyApiService.sendRequestWithSessionKey({
            supplierIDs: supplier_ids.join(','),
            getRowsForAllInvoices: 1,
            type: 'PRCORDER',
            recordsOnPage: records_on_page,
            stateID: 10,
            changedSince: last_hour
        }, session_key, 'getPurchaseDocuments', this.client_code).toPromise();

        if (first_page['records'].length > 0) {
            all_promises = all_promises.concat(first_page)
            if (first_page['status'].errorCode == 0) {
                pages = Math.ceil(first_page['status']['recordsTotal'] / records_on_page);

                if (pages > 1) {
                    for (let page = 2; page < pages + 1; page++) {
                        let page_promise = this.ErplyApiService.sendRequestWithSessionKey({
                            supplierIDs: supplier_ids.join(','),
                            getRowsForAllInvoices: 1,
                            type: 'PRCORDER',
                            recordsOnPage: records_on_page,
                            stateID: 10,
                            pageNo: page,
                            changedSince: last_hour
                            // changedSince: Math.floor((Date.now() - (this.INTERVAL * 60 * 1000)))
                        }, session_key, 'getPurchaseDocuments', this.client_code).toPromise()
                        all_promises.push(page_promise)
                    }
                }
            }
        }

        return all_promises
    }

    get(purchase_order_id: number): Observable<Object> {
        let session_key = this.getSessionKey()
        return this.ErplyApiService.sendRequestWithSessionKey({
            id: purchase_order_id,
        }, session_key, 'getPurchaseDocuments', this.client_code);
    }

    appendContractNumber(purchase_order_id: number, contract_number: string): Observable<Object> {
        let session_key = this.getSessionKey()
        return this.ErplyApiService.sendRequestWithSessionKey({
            id: purchase_order_id,
            attributeName1: "edi_contract_number",
            attributeType1: "text",
            attributeValue1: contract_number,
        }, session_key, 'savePurchaseDocument', this.client_code);
    }

    bulkUpdateEdiStatus(requests: any) {
        let session_key = this.getSessionKey()
        var i,j, temporary, chunk = 100;
        let promises = [];

        const awaitTimeout = delay =>
        new Promise(resolve => setTimeout(resolve, delay));
        for (i = 0,j = requests.length; i < j; i += chunk) {
            temporary = requests.slice(i, i + chunk);

            const promise = this.ErplyApiService.sendBulkRequestWithSessionKey(session_key, temporary, this.client_code).toPromise();
            promises.push(promise)
        }

        Promise.all(promises).then((values) => {
            return awaitTimeout(3600 + Math.random() * 2000).then(function () { // timeout to avoid socket hang up and too many requests error.
            }).catch((e)=> {
                console.log(values)
                console.log(e)
            })
        })

    }
}
