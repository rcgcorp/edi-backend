import { HttpModule, HttpService, Module } from '@nestjs/common';
import { ApiModule } from 'src/api/api.module';
import { ErplyPurchaseOrderService } from './erply-purchase-order/erply-purchase-order.service';
import { ErplySupplierService } from './erply-supplier/erply-supplier.service';
import { ErplyAddressService } from './erply-address/erply-address.service';
import { ErplyConfigService } from './erply-config/erply-config.service';
import { ErplyWarehouseService } from './erply-warehouse/erply-warehouse.service';

@Module({
  imports: [ApiModule, HttpModule],
  providers: [ErplyPurchaseOrderService, ErplySupplierService, ErplyAddressService, ErplyConfigService, ErplyWarehouseService],
  exports: [ErplyPurchaseOrderService, ErplySupplierService, ErplyAddressService, ErplyConfigService, ErplyWarehouseService],
})
export class ErplyModule {}
