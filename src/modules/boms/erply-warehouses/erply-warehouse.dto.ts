export class ErplyWarehouseDto {
    erply_warehouse_id: string;
    name: string;
    store_group: string;
    // group_name: string;
    phone: string|null;
    email: string|null;
    street: string|null;
    address2: string|null;
    city: string|null;
    state: string|null;
    postcode: string|null;
    country: string|null;
    // erply_date_time: Date;
}