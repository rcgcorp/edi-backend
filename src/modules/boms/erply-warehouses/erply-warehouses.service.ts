import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ErplyWarehouse } from 'src/entities/erply-warehouses.entity';
import { getConnection, Repository } from 'typeorm';
import { ErplyWarehouseDto } from './erply-warehouse.dto';

@Injectable()
export class ErplyWarehousesService {
    constructor(
        @InjectRepository(ErplyWarehouse)
        private erplyWarehouseRepository: Repository<ErplyWarehouse>,
    ) { }

    async find(params?: object) {
        return await this.erplyWarehouseRepository.findOne(params);
    }

    async findAll(params?: object) {
        return await this.erplyWarehouseRepository.find(params);
    }

    async create(dto: ErplyWarehouseDto) {
        const { erply_warehouse_id, name, store_group, phone, email, street, address2, city, state, postcode, country} = dto;
        let erplyWarehouse = new ErplyWarehouse();
        erplyWarehouse.erply_warehouse_id = erply_warehouse_id;
        erplyWarehouse.name = name;
        erplyWarehouse.store_group = store_group;
        erplyWarehouse.phone = phone;
        erplyWarehouse.email = email;
        erplyWarehouse.street = street;
        erplyWarehouse.address2 = address2;
        erplyWarehouse.city = city;
        erplyWarehouse.state = state;
        erplyWarehouse.postcode = postcode;
        erplyWarehouse.country = country;

        return await this.erplyWarehouseRepository.save(erplyWarehouse);
    }

    async update(warehouse: ErplyWarehouse, params: object) {
        return await getConnection()
            .createQueryBuilder()
            .update(ErplyWarehouse)
            .set(params)
            .where("erply_warehouse_id = :erply_warehouse_id", { erply_warehouse_id: warehouse.erply_warehouse_id })
            .execute();
    }

    async updateOrCreate(
        id: number,
        dto: ErplyWarehouseDto,
      ): Promise<ErplyWarehouse> {
        const { erply_warehouse_id, name, store_group, phone, email, street, address2, city, state, postcode, country} = dto;
        let warehouse = await this.find({ erply_warehouse_id: id });
        if (!warehouse) {
            warehouse = new ErplyWarehouse();
            warehouse.erply_warehouse_id = erply_warehouse_id;
          }
          warehouse.name = name;
          warehouse.store_group = store_group;
          warehouse.phone = phone;
          warehouse.email = email;
          warehouse.street = street;
          warehouse.address2 = address2;
          warehouse.city = city;
          warehouse.state = state;
          warehouse.postcode = postcode;
          warehouse.country = country;

          return await this.erplyWarehouseRepository.save(warehouse);
      }

}