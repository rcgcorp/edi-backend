import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ErplyWarehouse } from 'src/entities/erply-warehouses.entity';
import { ErplyWarehousesService } from './erply-warehouses.service';

@Module({
    imports: [ TypeOrmModule.forFeature([ErplyWarehouse])],
    providers: [ErplyWarehousesService],
    exports: [ErplyWarehousesService],
    controllers: []
})
export class ErplyWarehousesModule {}
