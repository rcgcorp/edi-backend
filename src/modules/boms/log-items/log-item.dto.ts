import { Timestamp } from "typeorm";

export class LogItemDto {
    purchase_order_id: number;
    reference_id: string;
    file_name?: string | null;
    message_function: string | null;
    contract_number?: string | null;
    notes?: string | null;
    status: string;
    type: string;
    total_lines?: number;
    total_amount?: number;
    total_amount_gst?: number;
    date: Timestamp | null;
    delivery_earliest_date?: Timestamp | null;
    delivery_latest_date?: Timestamp | null;
    // asn items
    split_or_complete?: string | null;
    consignment_note_number?: string | null;
    carrier_name?: string | null;
    shipment_tracking_number?: string | null;
    estimated_delivery_date?: Timestamp | null;
    fails_date_threshold?: boolean;
    fails_header_threshold?: boolean;
    log_buyer_code?: string | null;
    log_ship_to_code?: string | null;
    log_supplier_code?: string | null;
    escalated?: boolean;
    ship_date?: Timestamp | null;
}