import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ErplyApiService } from 'src/api/erply-api/erply-api.service';
import { LogItem } from 'src/entities/log-item.entity';
import { XmlBuilderDirector } from 'src/helpers/builders/xml/xml-builder.director';
import { Connection, getConnection, Repository, In} from 'typeorm';
import { LineItemsService } from '../line-items/line-items.service';
import { LogItemDto } from './log-item.dto';
import * as moment from 'moment-timezone'
import { PurchaseOrdersService } from 'src/modules/boms/purchase-orders/purchase-orders.service';

@Injectable()
export class LogItemsService {
    constructor(
        @InjectRepository(LogItem)
        private logItemRepository: Repository<LogItem>,
        private LineItemsService: LineItemsService,
        private XmlBuilderDirector: XmlBuilderDirector,
        private ErplyApiService: ErplyApiService,
        private PurchaseOrdersService: PurchaseOrdersService,
        private connection: Connection
    ) { }

    async find(params?: object) {
        return await this.logItemRepository.findOne(params);
    }

    async findAll(params?: object) {
        return await this.logItemRepository.find(params);
    }

    async create(dto: LogItemDto) {
        const { purchase_order_id, reference_id, file_name, message_function, status, type, notes, total_lines, total_amount, total_amount_gst,
            delivery_earliest_date, delivery_latest_date, date, split_or_complete, consignment_note_number, contract_number,
            carrier_name, shipment_tracking_number, estimated_delivery_date, escalated, fails_date_threshold, fails_header_threshold, ship_date,
            log_buyer_code, log_ship_to_code, log_supplier_code } = dto;
        let logItem = new LogItem();
        logItem.purchase_order_id = purchase_order_id;
        logItem.reference_id = reference_id;
        logItem.file_name = file_name;
        logItem.message_function = message_function;
        logItem.contract_number = contract_number;
        logItem.status = status;
        logItem.type = type;
        logItem.notes = notes;
        logItem.total_lines = total_lines;
        logItem.total_amount = total_amount;
        logItem.total_amount_gst = total_amount_gst;
        logItem.delivery_earliest_date = delivery_earliest_date;
        logItem.delivery_latest_date = delivery_latest_date;
        logItem.split_or_complete = split_or_complete;
        logItem.consignment_note_number = consignment_note_number;
        logItem.carrier_name = carrier_name;
        logItem.shipment_tracking_number = shipment_tracking_number;
        logItem.estimated_delivery_date = estimated_delivery_date;
        logItem.fails_date_threshold = fails_date_threshold;
        logItem.fails_header_threshold = fails_header_threshold;
        logItem.log_buyer_code = log_buyer_code;
        logItem.log_ship_to_code = log_ship_to_code;
        logItem.log_supplier_code = log_supplier_code;
        logItem.escalated = escalated;
        logItem.ship_date = ship_date;
        logItem.date = date
        return await this.logItemRepository.save(logItem);
    }

    async update(logItem: LogItem, params: object) {
        return await getConnection()
            .createQueryBuilder()
            .update(LogItem)
            .set(params)
            .where("id = :id", { id: logItem.id })
            .execute();
    }

    async rejectEvent(logItem: LogItem) {
        await this.update(logItem, { status: 'cancelled' })
        //Create a new purchase order log with the same parameters as above. Change the messag function code to cancel
        logItem.status = 'ready_to_send';
        logItem.notes = 'CANCEL';
        logItem.message_function = 'CANCEL';
        logItem.type = 'purchase_order';
        logItem.file_name = null;
        let new_log = await this.create(logItem);

        let items = await this.LineItemsService.findAll({ where: { log_id: logItem.id } })
        items.forEach(async item => {
            item.log_id = new_log.id;
            await this.LineItemsService.create(item);
        })

        await this.ErplyApiService.sendRequest({
            id: logItem.purchase_order.erply_base_id,
            stateID: 10,
            attributeName1: "edi_status",
            attributeType1: "int",
            attributeValue1: 5,
        }, 'savePurchaseDocument').toPromise();

        await this.PurchaseOrdersService.update(logItem.purchase_order, {
            status: 'rejected'
        })

        let log = await this.find({
            where: {
                id: logItem.id
            },
            relations: ['line_items'],
        })

        new_log = await this.find({
            where: {
                id: new_log.id
            },
            relations: ['line_items'],
        })

        return {
            log_item: log,
            new_log: new_log
        }
    }

    async bulkChangeEvent(poIds: Array<Number>, action: String) {
        let changePromises = []
        let log_items = await this.findAll({
            where: {
                purchase_order_id:  In(poIds),
                status: 'requires_action'
            },
            relations: ['purchase_order']
        });

        log_items.forEach (async log => {
            changePromises.push(new Promise(async (resolve, reject) => {
                try {
                    this[`${action}Event`](log);
                    resolve(log.purchase_order_id)
                } catch (err) {
                    reject(err)
                }
            }))
        })

        const awaitTimeout = delay =>
        new Promise(resolve => setTimeout(resolve, delay));


        return await Promise.all(changePromises).then((values) => {
            return awaitTimeout(3600 + Math.random() * 2000).then(function () {
                return values
            }).catch(err => {
                console.log(err)
            })
        })
    }

    async changeEvent(logItem: LogItem) {
        await this.update(logItem, { status: 'awaiting_changes' })
        //Create a new purchase order log with the same parameters as above. Change the message function code to change

        let log = await this.find({
            where: {
                id: logItem.id
            },
            relations: ['line_items'],
        })

        return {
            log_item: log
        }

    }

    async acceptEvent(log_item: LogItem) {
        await this.update(log_item, { status: 'change_accepted' })
        log_item.status = 'change_accepted';
        const po = log_item.purchase_order;
        await this.PurchaseOrdersService.update(po, {
            status: 'change_accepted'
        })

        let line_items = await this.LineItemsService.findAll({
            where: {
                log_id: log_item.id,
                fails_unknown_item: 0 // exclude unknown items.
            },
            order: {
                style_code: "ASC",
                size_code: "ASC",
            },
        });

        let requests = line_items.map(item => {
            return {
                requestName: 'getProducts',
                recordsOnPage: 1,
                code: item.style_code + (item.size_code ? `-${item.size_code}` : '')
            }
        });
        let chunkSize = 100;
        const request_chunks = requests.map((e, i) => {
            return i % chunkSize === 0 ? requests.slice(i, i + chunkSize) : null;
        }).filter(e => { return e; });


        let products = [];
        for (let i = 0; i < request_chunks.length; i++) {
            let request_chunk = JSON.stringify(request_chunks[i]);
            let response = await this.ErplyApiService.sendRequest({ requests: request_chunk }).toPromise();
            products = products.concat(response['requests']);
        }

        let update_data = {};
        let counter = 1;

        console.log("Line items count", line_items.length);

        for (const item of line_items) {
            let style_code = item.style_code + (item.size_code ? `-${item.size_code}` : '')
            let product = products.map(request => {
                if (request.records[0]) {
                    return request.records[0]
                }
                return {code: 0}
            }).find(product => product.code == style_code);
            if (!product) {
                //THROW SOMETHING HERE TO KILL EVERYTHING
                console.log('there is unknown product item')
                console.log(item)
            }

            if (item.delivered_qty > 0 && product) { // Only update Erply with delivered qty > 0 product.
                update_data[`productID${counter}`] = product.productID;
                update_data[`vatrateID${counter}`] = process.env.TAF_VATRATE_ID;
                update_data[`amount${counter}`] = item.delivered_qty;
                update_data[`price${counter}`] = item.delivered_unit_price;
                counter++;
            }
        }

        console.log("Product counts ", counter, po.erply_po_id);

        await this.ErplyApiService.sendRequest({
            id: po.erply_base_id,
            ...update_data,
            shipDate: moment(log_item.delivery_earliest_date.toString()).format('YYYY-MM-DD'),
            attributeName1: 'edi_status',
            attributeType1: 'int',
            attributeValue1: 3,
            attributeName2: 'edi_latest_delivery_date',
            attributeType2: 'text',
            attributeValue2: moment(log_item.delivery_latest_date.toString()).format('YYYY-MM-DD'),
        }, 'savePurchaseDocument').toPromise();

        let log = await this.find({
            where: {
                id: log_item.id
            },
            relations: ['line_items'],
        })

        return {
            log_item: log,
        }


    }

    async bulkExport(dates: Array<any>, supplier_ids: Array<any>, type: string) {
        let conditions = []
        if (dates.length)
            conditions.push(`lo.created_at BETWEEN '${moment(dates[0]).startOf('day').format('YYYY-MM-DD HH:mm:ss')}' AND '${moment(dates[1]).endOf('day').format('YYYY-MM-DD HH:mm:ss')}'`)
        if (supplier_ids.length)
            conditions.push(`po.supplier_id IN (${supplier_ids.join(',')})`)
        if (type !== '')
            conditions.push(`lo.type = '${type}'`)
        else {
            conditions.push("lo.type IN ('despatch_advice', 'invoice')")
        }

        if (type == 'despatch_advice'){
            conditions.push("li.sscc is not null")
        }

        

        let condition_string = "WHERE " + conditions.join(' AND ');

        const asn_counts = await this.connection.query(`
        SELECT po.erply_po_id, su.name, po.warehouse_name, lo.type, li.style_code, li.size_code, li.order_qty, li.unit_price, lo.reference_id, li.sscc, lo.split_or_complete, li.delivered_qty, li.delivered_unit_price,
        lo.date, lo.ship_date, lo.estimated_delivery_date,li.description as product_desc,lo.consignment_note_number as connote,lo.shipment_tracking_number,po.erply_no as tracking_number
        FROM purchase_order po 
         RIGHT JOIN supplier su ON po.supplier_id = su.id
        LEFT JOIN log_item lo ON po.id = lo.purchase_order_id
        LEFT JOIN line_item li ON lo.id = li.log_id
        ${condition_string} 
        GROUP BY po.erply_po_id, su.name, po.warehouse_name, li.style_code, li.size_code, li.order_qty, li.unit_price, lo.reference_id, li.sscc, lo.split_or_complete, li.delivered_qty, li.delivered_unit_price, lo.type
        ORDER BY lo.reference_id ASC, li.size_code ASC
        `);
        return asn_counts;

    }

    

    async bulkExportAwaitingResponse(dates: Array<any>, supplier_ids: Array<any>) {
        let conditions = []
        if (dates.length)
            conditions.push(`po.created_at BETWEEN '${moment(dates[0]).startOf('day').format('YYYY-MM-DD HH:mm:ss')}' AND '${moment(dates[1]).endOf('day').format('YYYY-MM-DD HH:mm:ss')}'`)
        if (supplier_ids.length)
            conditions.push(`po.supplier_id IN (${supplier_ids.join(',')})`)

        conditions.push(`po.status='awaiting_response'`)

        let condition_string = "WHERE " + conditions.join(' AND ');

        const asn_counts = await this.connection.query(`

            SELECT sp.name as supplier,po.warehouse_name  as store, po.erply_po_id as po_number, 
            po.type as type, po.status as status, po.created_at,li.delivery_earliest_date, po.erply_no as tracking_number, lit.style_code, 
            lit.size_code, lit.description, lit.order_qty
            FROM purchase_order po
            LEFT JOIN supplier sp ON sp.id = po.supplier_id
            LEFT JOIN log_item li ON li.purchase_order_id = po.id
            LEFT JOIN line_item lit	ON lit.log_id = li.id
            ${condition_string} 
            ORDER BY po.created_at ASC

        `);
        return asn_counts;

    }

    async bulkExportResponseReceived(dates: Array<any>, supplier_ids: Array<any>) {
        let conditions = []
        if (dates.length)
            conditions.push(`po.created_at BETWEEN '${moment(dates[0]).startOf('day').format('YYYY-MM-DD HH:mm:ss')}' AND '${moment(dates[1]).endOf('day').format('YYYY-MM-DD HH:mm:ss')}'`)
        if (supplier_ids.length)
            conditions.push(`po.supplier_id IN (${supplier_ids.join(',')})`)

        conditions.push(`po.status='response_received'`)
        conditions.push(`li.status='response_received'`)

        let condition_string = "WHERE " + conditions.join(' AND ');

        const asn_counts = await this.connection.query(`

            SELECT sp.name as supplier,po.warehouse_name  as store, po.erply_po_id as po_number, 
            po.type as type, po.status as status, po.created_at,li.delivery_earliest_date, po.erply_no as tracking_number, lit.style_code, 
            lit.size_code, lit.description, lit.order_qty
            FROM purchase_order po
            LEFT JOIN supplier sp ON sp.id = po.supplier_id
            LEFT JOIN log_item li ON li.purchase_order_id = po.id
            LEFT JOIN line_item lit	ON lit.log_id = li.id
            ${condition_string} 
            ORDER BY po.created_at ASC

        `);
        return asn_counts;

    }

    async generateXMLFile(log: LogItem) {
        if (log.file_name == null) {
            let po = log.purchase_order;
            return await this.XmlBuilderDirector.buildPurchaseOrderXML(po,
                {
                    address: po.supplier_address,
                    city: po.supplier_city,
                    state: po.supplier_state,
                    postcode: po.supplier_postcode,
                    country: po.supplier_country,
                    email: po.supplier_email,
                    phone: po.supplier_phone
                },
                {
                    address: po.address,
                    address2: po.address2,
                    city: po.city,
                    state: po.state,
                    postcode: po.postcode,
                    country: po.country,
                    email: po.email,
                    phone: po.phone
                },
                log)
        }
        else {
            let fs = require('fs');
            let content = fs.readFileSync('./temp/archive/' + log.file_name, 'utf-8');
            return content;
        }
    }

}
