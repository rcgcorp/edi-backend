import { Body, Controller, Get, Post, Param, Put, Query, UseGuards } from '@nestjs/common';
import { JwtGuard } from 'src/modules/auth/jwt.guard';
import { LogItemsService } from './log-items.service';

@Controller('log-items')
export class LogItemsController {
    constructor(private LogItemsService: LogItemsService) { }

    @Put('action/:id')
    @UseGuards(JwtGuard)
    async update(@Param() params, @Body() body) {
        const log = await this.LogItemsService.find({
            where: {
                id: params.id,
            },
            relations: ['purchase_order']
        });
        return await this.LogItemsService[`${body.status}Event`](log);
    }

    @Get(':id/xml')
    async xml(
        @Param() params
    ) {
        const log = await this.LogItemsService.find({
            where: {
                id: params.id,
            },
            relations: ['line_items', 'purchase_order', 'purchase_order.supplier']
        });
        let xml = await this.LogItemsService.generateXMLFile(log);
        return xml;
    }

    @Get('/bulk/export')
    async bulkExport(
        @Query('supplier_ids') supplier_ids = [],
        @Query('type') type = null,
        @Query('dates') dates = []
    ) {
        return await this.LogItemsService.bulkExport(dates, supplier_ids, type);
    }

    @Get('/bulk/exportawaitingresponse')
    async bulkExportAwaitingResponse(
        @Query('supplier_ids') supplier_ids = [],
        @Query('dates') dates = []
    ) {
        return await this.LogItemsService.bulkExportAwaitingResponse(dates, supplier_ids);
    }

    @Get('/bulk/exportresponsereceived')
    async bulkExportResponseReceived(
        @Query('supplier_ids') supplier_ids = [],
        @Query('dates') dates = []
    ) {
        return await this.LogItemsService.bulkExportResponseReceived(dates, supplier_ids);
    }


    @Post('bulktakeactions')
    @UseGuards(JwtGuard)
    async bulkTakeActions(
        @Body() params
    ) {
        const results = await this.LogItemsService.bulkChangeEvent(params.po_ids, params.action)
        return results
    }
}
