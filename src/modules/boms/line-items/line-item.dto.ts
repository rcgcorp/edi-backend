import { Timestamp } from "typeorm";

export class LineItemDto {
    log_id: number;
    product_id?: number;
    gtin?: string;
    style_code?: string;
    size_code?: string;
    description?: string;
    sscc?: string;
    colour_code?: string|null;
    order_qty: number;
    delivered_qty?: number;
    backordered_qty?: number;
    unit_price?: number;
    delivered_unit_price?: number;
    tax_rate?: number;
    fails_quantity_threshold?: boolean;
    fails_price_threshold?: boolean;
    fails_backorder_threshold?: boolean;
    fails_missing_item?: boolean;
    fails_unknown_item?: boolean;
    backorder_date?: Timestamp;
    fails_quantity_within_threshold?: boolean;
    fails_price_within_threshold?: boolean;
    // erply_date_time: Date;
}