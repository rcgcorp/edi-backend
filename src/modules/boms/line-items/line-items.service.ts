import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { LineItem } from 'src/entities/line-item.entity';
import { getConnection, Repository } from 'typeorm';
import { LineItemDto } from './line-item.dto';

@Injectable()
export class LineItemsService {
    constructor(
        @InjectRepository(LineItem)
        private lineItemRepository: Repository<LineItem>,
    ) { }

    async find(params?: object) {
        return await this.lineItemRepository.findOne(params);
    }

    async findAll(params?: object) {
        return await this.lineItemRepository.find(params);
    }

    async create(dto: LineItemDto) {
        const { log_id, product_id, gtin, style_code, description, sscc, colour_code, size_code, order_qty, delivered_qty, backordered_qty, unit_price, delivered_unit_price, tax_rate, 
            fails_price_threshold, fails_quantity_within_threshold,fails_price_within_threshold, fails_quantity_threshold, fails_backorder_threshold, fails_missing_item, fails_unknown_item, backorder_date  } = dto;
        let lineItem = new LineItem();
        lineItem.log_id = log_id;
        lineItem.product_id = product_id;
        lineItem.gtin = gtin;
        lineItem.style_code = style_code;
        lineItem.size_code = size_code;
        lineItem.description = description;
        lineItem.sscc = sscc;
        lineItem.colour_code = colour_code;
        lineItem.order_qty = order_qty;
        lineItem.delivered_qty = delivered_qty;
        lineItem.backordered_qty = backordered_qty;
        lineItem.unit_price = unit_price;
        lineItem.delivered_unit_price = delivered_unit_price;
        lineItem.fails_price_threshold = fails_price_threshold;
        lineItem.fails_quantity_threshold = fails_quantity_threshold;
        lineItem.fails_backorder_threshold = fails_backorder_threshold;
        lineItem.fails_missing_item = fails_missing_item;
        lineItem.fails_unknown_item = fails_unknown_item;
        lineItem.backorder_date = backorder_date;
        lineItem.tax_rate = tax_rate;
        lineItem.fails_quantity_within_threshold = fails_quantity_within_threshold;
        lineItem.fails_price_within_threshold = fails_price_within_threshold;
        return await this.lineItemRepository.save(lineItem);
    }

    async update(lineItem: LineItem, params: object) {
        return await getConnection()
            .createQueryBuilder()
            .update(LineItem)
            .set(params)
            .where("id = :id", { id: lineItem.id })
            .execute();
    }

}
