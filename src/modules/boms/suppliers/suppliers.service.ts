import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, In, Repository } from 'typeorm';
import { Supplier } from 'src/entities/supplier.entity';
import { SupplierDto } from './supplier.dto';
import {
    paginate,
    Pagination,
    IPaginationOptions,
} from 'nestjs-typeorm-paginate';
import { Like } from "typeorm";
import { EmailRecipient } from 'src/entities/email-recipient.entity';
import { Document } from 'src/entities/document.entity';

@Injectable()
export class SuppliersSerrvice {
    constructor(
        @InjectRepository(Supplier)
        private supplierRepository: Repository<Supplier>,
        @InjectRepository(EmailRecipient)
        private emailRepository: Repository<EmailRecipient>,
        @InjectRepository(Document)
        private documentRepository: Repository<Document>,
    ) { }

    async find(params?: object): Promise<Supplier> {
        return await this.supplierRepository.findOne(params);
    }

    async findAll(params?: object): Promise<Array<Supplier>> {
        return await this.supplierRepository.find(params);
    }

    async paginate(options: IPaginationOptions, search: string, client_code: number): Promise<Pagination<Supplier>> {
        console.log("Searching", search)
        return paginate<Supplier>(this.supplierRepository, options, {
            where: {
                name: Like(`%${search}%`),
                client_code: client_code
            },
            order: {
                name: "ASC"
            },
        });
    }


    async create(dto: SupplierDto, clientCode: number): Promise<Supplier> {
        try {
            const { erply_supplier_id, name, alias, quantity_variation, price_variation, sftp_endpoint, sftp_protocol, sftp_password, sftp_username, edi_client, orders_sync, responses_sync, dispatch_advices_sync, email_recipients } = dto;
            let supplier = new Supplier();
            supplier.erply_supplier_id = erply_supplier_id;
            supplier.name = name;
            supplier.alias = alias;
            supplier.quantity_variation = quantity_variation;
            supplier.price_variation = price_variation;
            supplier.sftp_endpoint = sftp_endpoint;
            supplier.sftp_protocol = sftp_protocol;
            supplier.sftp_password = sftp_password;
            supplier.sftp_username = sftp_username;
            supplier.edi_client_code = edi_client;
            supplier.client_code = clientCode;
            // supplier.orders_sync = orders_sync;
            // supplier.order_responses_sync = order_responses_sync;
            // supplier.dispatch_advice_sync = dispatch_advice_sync;

            let document_keys = [];
            if (orders_sync)
                document_keys.push('orders_sync');
            if (responses_sync)
                document_keys.push('responses_sync');
            if (dispatch_advices_sync)
                document_keys.push('dispatch_advices_sync');

            let documents = await this.documentRepository.find({
                where: { name: In(document_keys) },
            })

            supplier.documents = documents;


            supplier = await this.supplierRepository.save(supplier);

            for (const recipient of email_recipients) {
                const email_recipient = new EmailRecipient();
                email_recipient.email = recipient;
                email_recipient.supplier_id = supplier.id;
                await this.emailRepository.save(email_recipient);
            }

            return supplier;
        } catch (e) {
            console.log(e);
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: e.sqlMessage,
            }, HttpStatus.BAD_REQUEST);
        }
    }

    async update(supplier: Supplier, dto: SupplierDto) {

        try {
            const { alias, quantity_variation, price_variation, sftp_endpoint, sftp_protocol, sftp_password, sftp_username, edi_client, orders_sync, responses_sync, dispatch_advices_sync, email_recipients } = dto;
            supplier.alias = alias;
            supplier.sftp_endpoint = sftp_endpoint;
            supplier.sftp_protocol = sftp_protocol;
            supplier.sftp_password = sftp_password;
            supplier.sftp_username = sftp_username;
            supplier.edi_client_code = edi_client;
            supplier.quantity_variation = quantity_variation;
            supplier.price_variation = price_variation;
            // supplier.orders_sync = orders_sync;
            // supplier.order_responses_sync = order_responses_sync;
            // supplier.dispatch_advice_sync = dispatch_advice_sync;
            let document_keys = [];
            if (orders_sync)
                document_keys.push('orders_sync');
            if (responses_sync)
                document_keys.push('responses_sync');
            if (dispatch_advices_sync)
                document_keys.push('dispatch_advices_sync');

            let documents = await this.documentRepository.find({
                where: { name: In(document_keys) },
            })

            supplier.documents = documents;

            supplier = await this.supplierRepository.save(supplier);
            await this.emailRepository.remove(supplier.email_recipients);

            supplier.email_recipients = [];
            for (const recipient of email_recipients) {
                const email_recipient = new EmailRecipient();
                email_recipient.email = recipient;
                email_recipient.supplier_id = supplier.id;
                await this.emailRepository.save(email_recipient);
                supplier.email_recipients.push(email_recipient)
            }

            return supplier;
        } catch (e) {
            console.log(e);
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: e.sqlMessage,
            }, HttpStatus.BAD_REQUEST);
        }
    }

    async getRecipients(supplier_id: any): Promise<any> {
        let supplier = await this.find({
            where: {
                erply_supplier_id: supplier_id
            },
            relations: ['email_recipients']
        })

        let recipients = (supplier.email_recipients.map(recipient => {
            return recipient.email
        }));

        return recipients;
    }
}
