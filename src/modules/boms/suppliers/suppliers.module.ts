import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApiModule } from 'src/api/api.module';
import { Document } from 'src/entities/document.entity';
import { EmailRecipient } from 'src/entities/email-recipient.entity';
import { Supplier } from 'src/entities/supplier.entity';
import { ErplySupplier } from 'src/entities/erply-suppliers.entity';
import { ErplyWarehouse } from 'src/entities/erply-warehouses.entity';
import { MailModule } from 'src/helpers/mail/mail.module';
import { ErplyModule } from 'src/modules/erply/erply.module';
import { SystemModule } from 'src/modules/system/system.module';
import { RetrievePurchaseOrdersService } from 'src/schedules/erply/get-purchase-orders/retrieve-purchase-orders.service';
import { PurchaseOrdersModule } from '../purchase-orders/purchase-orders.module';
import { SuppliersController } from './suppliers.controller';
import { SuppliersSerrvice } from './suppliers.service';
import { ErplyWarehousesService } from 'src/modules/boms/erply-warehouses/erply-warehouses.service';
import { ErplySuppliersService } from 'src/modules/boms/erply-suppliers/erply-suppliers.service';
import { AutoAcceptedOrderService } from 'src/schedules/erply/auto-accepted-orders/auto-accepted-orders.service';


@Module({
    imports: [ TypeOrmModule.forFeature([Supplier, EmailRecipient, Document, ErplySupplier, ErplyWarehouse]), SystemModule, ErplyModule, ApiModule, PurchaseOrdersModule, MailModule],
    providers: [SuppliersSerrvice, RetrievePurchaseOrdersService, ErplySuppliersService, ErplyWarehousesService, AutoAcceptedOrderService],
    exports: [SuppliersSerrvice],
    controllers: [SuppliersController]
})
export class SuppliersModule {}
