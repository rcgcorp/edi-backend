export class SupplierDto {
    erply_supplier_id: number;
    name: string;
    alias: string | null;
    price_variation: number | null;
    quantity_variation: number | null;
    sftp_endpoint: string | null;
    sftp_protocol: string | null;
    sftp_username: string | null;
    sftp_password: string | null;
    edi_client: string | null;
    orders_sync: boolean;
    responses_sync: boolean;
    dispatch_advices_sync: boolean;
    email_recipients: Array<any>;
    // erply_date_time: Date;
}