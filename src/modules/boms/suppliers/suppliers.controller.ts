import { Controller, Get, Post, Response, ParseIntPipe, Query, Req, Param, Body, Put, UseGuards, Headers, Res, HttpStatus, HttpException } from '@nestjs/common';
import { Pagination } from 'nestjs-typeorm-paginate';
import { catchError } from 'rxjs/operators';
import { Supplier } from 'src/entities/supplier.entity';
import { JwtGuard } from 'src/modules/auth/jwt.guard';
import { SystemService } from 'src/modules/system/system.service';
import { GetPurchaseOrdersService } from 'src/schedules/erply/get-purchase-orders/get-purchase-orders.service';
import { RetrievePurchaseOrdersService } from 'src/schedules/erply/get-purchase-orders/retrieve-purchase-orders.service';
import { AutoAcceptedOrderService } from 'src/schedules/erply/auto-accepted-orders/auto-accepted-orders.service';
import { SuppliersSerrvice } from './suppliers.service';

@Controller('suppliers')
export class SuppliersController {
    constructor(
        private SuppliersSerrvice: SuppliersSerrvice,
        private SystemService: SystemService,
        private RetrievePurchaseOrdersService: RetrievePurchaseOrdersService,
        private AutoAcceptedOrderService: AutoAcceptedOrderService
    ) { }

    @Get()
    @UseGuards(JwtGuard)
    async index(
        @Query('page', ParseIntPipe) page = 1,
        @Query('limit', ParseIntPipe) limit = 10,
        @Query('search') search = null,
        @Headers('clientCode') clientCode: number
    ): Promise<Pagination<Supplier>> {
        limit = limit > 100 ? 100 : limit;
        return this.SuppliersSerrvice.paginate(
            {
                page,
                limit,
            },
            search,
            clientCode
        );
    }

    @Get(':id')
    @UseGuards(JwtGuard)
    async find(@Param() params) {
        return this.SuppliersSerrvice.find({
            where: {
                id: params.id,
            },
            relations: ['email_recipients','documents'],
        });
    }

    @Post()
    @UseGuards(JwtGuard)
    async create(
        @Body() params,
        @Headers('clientCode') clientCode: number
    ) {

        try {
            return this.SuppliersSerrvice.create(params, clientCode);
        } catch (e) {
            console.log("Message", e.message)
        }
    }

    @Put(':id')
    @UseGuards(JwtGuard)
    async update(@Param() params, @Body() body) {
        const supplier = await this.SuppliersSerrvice.find({
            where: {
                id: params.id,
            },
            relations: ['email_recipients'],
        });
        return this.SuppliersSerrvice.update(supplier, body);
    }

    @Post('resync')
    @UseGuards(JwtGuard)
    async resyncPurchaseOrders(@Param() params, @Response() res) {
        try {
            let state = await this.SystemService.find({
                where: {
                    name: 'pos_sync'
                }
            });

            if(state.active) {
                res
                .status(HttpStatus.BAD_REQUEST)
                .send( {
                    state: 0,
                    message: "System is currently already syncing data."
                });
            }else {
                console.log("Syncing data Manually.");
                await this.RetrievePurchaseOrdersService.retrievePucharseOrders();
                await this.AutoAcceptedOrderService.autoAccepted();
                res.send({
                    state: 1,
                    message: "Data successfully updated"
                })
            }
        } catch (e) {
            throw e;
        }
    }
}
