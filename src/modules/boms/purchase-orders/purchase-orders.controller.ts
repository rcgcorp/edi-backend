import { Controller, Get, Headers, Param, ParseIntPipe, Query, UseGuards } from '@nestjs/common';
import { Pagination } from 'nestjs-typeorm-paginate';
import { PurchaseOrder } from 'src/entities/purchase-order.entity';
import { JwtGuard } from 'src/modules/auth/jwt.guard';
import { PurchaseOrdersService } from './purchase-orders.service';

@Controller('purchase-orders')
export class PurchaseOrdersController {
    constructor(private PurchaseOrdersService: PurchaseOrdersService) { }

    @Get()
    @UseGuards(JwtGuard)
    async index(
        @Query('page', ParseIntPipe) page = 1,
        @Query('limit', ParseIntPipe) limit = 10,
        @Query('search') search = null,
        @Query('statuses') statuses = [],
        @Query('types') types = [],
        @Query('dates') dates = [],
        @Query('supplier_id') supplier_id: number
    ): Promise<Pagination<PurchaseOrder>> {
        limit = limit > 100 ? 100 : limit;
        return this.PurchaseOrdersService.paginate(
            {
                page,
                limit,
            },
            search,
            statuses,
            types,
            dates,
            supplier_id
        );
    }
}
