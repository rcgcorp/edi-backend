import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmailRecipient } from 'src/entities/email-recipient.entity';
import { PurchaseOrder } from 'src/entities/purchase-order.entity';
import { Supplier } from 'src/entities/supplier.entity';
import { PurchaseOrdersService } from './purchase-orders.service';
import { PurchaseOrdersController } from './purchase-orders.controller';
import { LogItem } from 'src/entities/log-item.entity';
import { LineItem } from 'src/entities/line-item.entity';
import { LogItemsService } from '../log-items/log-items.service';
import { LineItemsService } from '../line-items/line-items.service';
import { LogItemsController } from '../log-items/log-items.controller';
import { XmlBuilderDirector } from 'src/helpers/builders/xml/xml-builder.director';
import { XmlBuilderModule } from 'src/helpers/builders/xml/xml-builder.module';
import { ApiModule } from 'src/api/api.module';

@Module({
    imports: [TypeOrmModule.forFeature([PurchaseOrder, LogItem, LineItem]), XmlBuilderModule, ApiModule],
    providers: [PurchaseOrdersService, LogItemsService, LineItemsService],
    exports: [PurchaseOrdersService, LogItemsService, LineItemsService,],
    controllers: [PurchaseOrdersController, LogItemsController],
})
export class PurchaseOrdersModule { }
