import { Timestamp } from "typeorm";

export class PurchaseOrderDto {
    erply_po_id: number;
    erply_base_id?: number;
    erply_no?: string;
    erply_blanket_id: number|null
    supplier_id: number;
    warehouse_id: number;
    warehouse_name: string;
    contract_number?: string;
    address: string;
    address2?: string;
    city: string;
    state: string;
    country: string;
    postcode: string;
    phone: string;
    email: string;
    supplier_address: string;
    supplier_city: string;
    supplier_state: string;
    supplier_country: string;
    supplier_postcode: string;
    supplier_phone: string;
    supplier_email: string;
    type: string;
    status: string;
    erply_created_at: Number;
    erply_last_synced: Number;
    // erply_date_time: Date;
}