import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PurchaseOrder } from 'src/entities/purchase-order.entity';
import { Between, getConnection, In, Like, Repository } from 'typeorm';
import { PurchaseOrderDto } from './purchase-order.dto';
import { Cron } from '@nestjs/schedule';
import { startOfDay, endOfDay } from 'date-fns';
import { IPaginationOptions, paginate, Pagination } from 'nestjs-typeorm-paginate';
import * as moment from "moment-timezone";

@Injectable()
export class PurchaseOrdersService {
    constructor(
        @InjectRepository(PurchaseOrder)
        private purchaseRepository: Repository<PurchaseOrder>,
    ) { }

    async find(params?: object) {
        return await this.purchaseRepository.findOne(params);
    }

    async findAll(params?: object) {
        return await this.purchaseRepository.find(params);
    }

    async paginate(options: IPaginationOptions, search: string, statuses: Array<any>, types: Array<any>, dates: Array<any>, supplier_id: number): Promise<Pagination<PurchaseOrder>> {
        console.log("Searching", search)
        let status_query = statuses.length ? {
            status: In(statuses)
        } : {};
        let type_query = types.length ? {
            type: In(types)
        } : {};
        let date_query = dates.length ? {
            created_at: Between(moment(dates[0]).startOf('day').format('YYYY-MM-DD HH:mm:ss'), moment(dates[1]).endOf('day').format('YYYY-MM-DD HH:mm:ss'))
        } : {};

        let search_query = search && search != '' ? [
            {
                supplier_id: supplier_id,
                erply_po_id: Like(`%${search}%`),
                ...status_query,
                ...type_query,
                ...date_query
            },
            {
                supplier_id: supplier_id,
                warehouse_name: Like(`%${search}%`),
                ...status_query,
                ...type_query,
                ...date_query
            },
            {
                supplier_id: supplier_id,
                erply_base_id: Like(`%${search}%`),
                ...status_query,
                ...type_query,
                ...date_query
            },
        ] : {
            supplier_id: supplier_id,
            ...status_query,
            ...type_query,
            ...date_query
        }

        return paginate<PurchaseOrder>(this.purchaseRepository, options, {
            where: search_query,
            relations: ['supplier', 'log_items', 'log_items.line_items'],
            order: {
                erply_po_id: "DESC"
            },
        });
    }

    async create(dto: PurchaseOrderDto) {
        let purchaseOrder = new PurchaseOrder();
        purchaseOrder.erply_po_id = dto.erply_po_id;
        purchaseOrder.erply_base_id = dto.erply_base_id;
        purchaseOrder.erply_no = dto.erply_no;
        purchaseOrder.erply_blanket_id = dto.erply_blanket_id;
        purchaseOrder.supplier_id = dto.supplier_id;
        purchaseOrder.warehouse_id = dto.warehouse_id;
        purchaseOrder.warehouse_name = dto.warehouse_name;
        purchaseOrder.contract_number = dto.contract_number;
        purchaseOrder.address = dto.address;
        purchaseOrder.address2 = dto.address2;
        purchaseOrder.city = dto.city;
        purchaseOrder.state = dto.state;
        purchaseOrder.country = dto.country;
        purchaseOrder.postcode = dto.postcode;
        purchaseOrder.phone = dto.phone;
        purchaseOrder.email = dto.email;
        purchaseOrder.supplier_address = dto.supplier_address;
        purchaseOrder.supplier_city = dto.supplier_city;
        purchaseOrder.supplier_state = dto.supplier_state;
        purchaseOrder.supplier_country = dto.supplier_country;
        purchaseOrder.supplier_postcode = dto.supplier_postcode;
        purchaseOrder.supplier_phone = dto.supplier_phone;
        purchaseOrder.supplier_email = dto.supplier_email;
        purchaseOrder.status = dto.status;
        purchaseOrder.type = dto.type;
        purchaseOrder.erply_created_at = dto.erply_created_at;
        purchaseOrder.erply_last_synced = dto.erply_last_synced;
        return await this.purchaseRepository.save(purchaseOrder);
    }

    async update(purchaseOrder: PurchaseOrder, params: object) {
        return await getConnection()
            .createQueryBuilder()
            .update(PurchaseOrder)
            .set(params)
            .where("id = :id", { id: purchaseOrder.id })
            .execute();
    }
}
