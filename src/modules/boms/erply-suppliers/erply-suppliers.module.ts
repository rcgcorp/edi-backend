import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ErplySupplier } from 'src/entities/erply-suppliers.entity';
import { ErplySuppliersService } from './erply-suppliers.service';
import { ErplySuppliersController } from './erply-suppliers.controller';

@Module({
    imports: [ TypeOrmModule.forFeature([ErplySupplier])],
    providers: [ErplySuppliersService],
    exports: [ErplySuppliersService],
    controllers: [ErplySuppliersController]
})
export class ErplySuppliersModule {}
