import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ErplySupplier } from 'src/entities/erply-suppliers.entity';
import { getConnection, Repository } from 'typeorm';
import { ErplySupplierDto } from './erply-supplier.dto';
import {
  paginate,
  Pagination,
  IPaginationOptions,
} from 'nestjs-typeorm-paginate';
import { Like } from 'typeorm';

@Injectable()
export class ErplySuppliersService {
  constructor(
    @InjectRepository(ErplySupplier)
    private supplierRepository: Repository<ErplySupplier>,
  ) { }

  async find(params?: object): Promise<ErplySupplier> {
    return await this.supplierRepository.findOne(params);
  }

  async findAll(params?: object): Promise<Array<ErplySupplier>> {
    return await this.supplierRepository.find(params);
  }

  async paginate(
    options: IPaginationOptions,
    search: string,
  ): Promise<Pagination<ErplySupplier>> {
    console.log('Searching', search);
    return paginate<ErplySupplier>(this.supplierRepository, options, {
      where: {
        name: Like(`%${search}%`),
      },
    });
  }

  async create(dto: ErplySupplierDto): Promise<ErplySupplier> {
    const {
      erply_supplier_id,
      name,
      type,
      group_id,
      phone,
      email,
      address,
      city,
      state,
      postcode,
      country,
    } = dto;
    const supplier = new ErplySupplier();
    supplier.erply_supplier_id = erply_supplier_id;
    supplier.name = name;
    supplier.type = type;
    supplier.group_id = group_id;
    // supplier.group_name = group_name;
    supplier.phone = phone;
    supplier.email = email;
    supplier.address = address;
    supplier.city = city;
    supplier.state = state;
    supplier.postcode = postcode;
    supplier.country = country;
    return await this.supplierRepository.save(supplier);
  }

  async updateOrCreate(
    id: number,
    dto: ErplySupplierDto,
  ): Promise<ErplySupplier> {
    const {
      erply_supplier_id,
      name,
      type,
      group_id,
      phone,
      email,
      address,
      city,
      state,
      postcode,
      country,
    } = dto;
    let supplier = await this.find({ erply_supplier_id: id });
    if (!supplier) {
      supplier = new ErplySupplier();
      supplier.erply_supplier_id = erply_supplier_id;
    }
    supplier.name = name;
    supplier.type = type;
    supplier.group_id = group_id;
    // supplier.group_name = group_name;
    supplier.phone = phone;
    supplier.email = email;
    supplier.address = address;
    supplier.city = city;
    supplier.state = state;
    supplier.postcode = postcode;
    supplier.country = country;
    return await this.supplierRepository.save(supplier);
  }

  async update(supplier: ErplySupplier, params: object) {
    return await getConnection()
      .createQueryBuilder()
      .update(ErplySupplier)
      .set(params)
      .where('id = :id', { id: supplier.erply_supplier_id })
      .execute();
  }
}
