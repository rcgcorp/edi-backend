export class ErplySupplierDto {
    erply_supplier_id: number;
    name: string;
    type: string;
    group_id: number;
    // group_name: string;
    phone: string;
    email: string;
    address: string|null;
    city: string|null;
    state: string|null;
    postcode: string|null;
    country: string|null;
    // erply_date_time: Date;
}