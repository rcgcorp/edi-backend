import {
    Controller, Get, Post, Response, ParseIntPipe, Query, Req, Param, Body, UseGuards,
} from '@nestjs/common';
import { ErplySuppliersService } from './erply-suppliers.service';
import { Pagination } from 'nestjs-typeorm-paginate';
import { ErplySupplier } from 'src/entities/erply-suppliers.entity';
import { Request } from 'express';
import { JwtGuard } from 'src/modules/auth/jwt.guard';

@Controller('erply-suppliers')
export class ErplySuppliersController {
    constructor(private ErplySuppliersService: ErplySuppliersService) { }

    @UseGuards(JwtGuard)
    @Get()
    async index(
        @Query('page', ParseIntPipe) page = 1,
        @Query('limit', ParseIntPipe) limit = 10,
        @Query('search') search = null,
    ): Promise<Pagination<ErplySupplier>> {
        limit = limit > 100 ? 100 : limit;
        return this.ErplySuppliersService.paginate(
            { page, limit, },
            search,
        );
    }

    @Post()
    async create(@Body() params) {
        return params;
    }
}
