import { Body, Controller, Get, HttpStatus, Post, Query, Response } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { LocalAuthGuard } from '../local-auth.guard';
import { SigninService } from './signin.service';

@Controller('auth/signin')
export class SigninController {
    constructor(
        private SigninService: SigninService,
        private jwtService: JwtService,
    ) { }


    @Get()
    get(@Response() res) {
        res.send({ message: 'yes' })
    }

    @Post()
    async post(@Response() res, @Body() params) {
        console.log(params);
        this.SigninService.validateUser(
            params.client_code,
            params.username,
            params.password
        ).subscribe(async response => {
            response = response['data']
            console.log(response);
            const status = response['status']['responseStatus'];
            if (status != 'ok') {
                res
                    .status(HttpStatus.FORBIDDEN)
                    .send({
                        message: `Login failed. Please check your username and password (${response['status']['errorCode']})`,
                        status: HttpStatus.FORBIDDEN
                    });
            } else {
                const user = response['records'][0];
                res.send({
                    status: status, user: user, payload: this.jwtService.sign({
                        client_code: params.client_code,
                        username: params.username
                    })
                });

            }
        });
    }
}
