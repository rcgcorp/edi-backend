import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';
import { ErplyConfigService } from 'src/modules/erply/erply-config/erply-config.service';
const qs = require('querystring');

@Injectable()
export class SigninService {
    constructor(
        private jwtService: JwtService,
        private erplyConfigService: ErplyConfigService
    ) { }

    validateUser(
        clientCode: number,
        username: string,
        pass: string,
    ): Observable<object> {
        return this.erplyConfigService.verifyUser(
            clientCode,
            username,
            pass,
        )

    }
}
