import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { SigninService } from './signin/signin.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private SigninService: SigninService) {
    super({
      usernameField: 'username',
      passwordField: 'password',
      passReqToCallback: true,
    });
  }

  async validate(
    req: Object,
    username: string,
    password: string,
  ): Promise<any> {
    console.log('data here', req);
    console.log('yes');
    const clientCode = req['body'].clientCode;
    const user = await this.SigninService.validateUser(
      clientCode,
      username,
      password,
    );
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
