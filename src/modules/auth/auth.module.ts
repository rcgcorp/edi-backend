import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { ErplyModule } from '../erply/erply.module';
import { JwtStrategy } from './jwt.strategy';
import { LocalStrategy } from './local.strategy';
import { SigninController } from './signin/signin.controller';
import { SigninService } from './signin/signin.service';

@Module({
  imports: [ErplyModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET'),
        signOptions: { expiresIn: '2 days' },
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [SigninController],
  providers: [SigninService, JwtStrategy, LocalStrategy]
})
export class AuthModule { }
