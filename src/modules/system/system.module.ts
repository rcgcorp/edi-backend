import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SystemState } from 'src/entities/system-state.entity';
import { SystemService } from './system.service';
import { SystemController } from './system.controller';
import { SystemEmailRecipient } from 'src/entities/system-email-recipient.entity';
import { EmailRecipient } from 'src/entities/email-recipient.entity';
import { LogError } from 'src/entities/log-error.entity';
import { SystemErrorLog } from 'src/entities/system-error-log.entity';

@Module({
  imports: [TypeOrmModule.forFeature([SystemState, SystemEmailRecipient, LogError, EmailRecipient, SystemErrorLog])],
  providers: [SystemService],
  exports: [SystemService],
  controllers: [SystemController]
})
export class SystemModule {}
