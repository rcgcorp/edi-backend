import { Body, Controller, Get, Post, Put, Response } from '@nestjs/common';
import { SystemService } from './system.service';

@Controller('system')
export class SystemController {

    constructor(
        private SystemService: SystemService,
    ) { }

    @Get("/pos_sync")
    async getXMLTwo(@Response() res) {
        let state = await this.SystemService.find({
            where: {
                name: 'pos_sync'
            }
        });

        res.send(state)
    }

    @Get("/system_emails")
    async getSystemEmails(@Response() res) {
        let state = await this.SystemService.findEmails();
        res.send(state)
    }

    @Put("/system_emails")
    async saveSystemEmails(@Response() res, @Body() params,) {
        await this.SystemService.saveEmails(params.buyers_emails, 'buyers');
        await this.SystemService.saveEmails(params.errors_emails, 'errors');
        res.send('Emails successfully saved')
    }
}
