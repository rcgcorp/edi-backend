import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { LogError } from 'src/entities/log-error.entity';
import { SystemEmailRecipient } from 'src/entities/system-email-recipient.entity';
import { EmailRecipient } from 'src/entities/email-recipient.entity';
import { SystemState } from 'src/entities/system-state.entity';
import { SystemErrorLog } from 'src/entities/system-error-log.entity';
import { Connection, getConnection, Repository } from 'typeorm';

@Injectable()
export class SystemService {
    constructor(
        @InjectRepository(SystemState)
        private systemStateRepository: Repository<SystemState>,
        @InjectRepository(SystemEmailRecipient)
        private systemEmailRepository: Repository<SystemEmailRecipient>,
        @InjectRepository(LogError)
        private logErrorRepository: Repository<LogError>,
        @InjectRepository(EmailRecipient)
        private supplicerEmailRecipientRepository: Repository<EmailRecipient>,
        @InjectRepository(SystemErrorLog)
        private systemErrorLogRepository: Repository<SystemErrorLog>,
        private connection: Connection
    ) { }

    async find(params?: object) {
        return await this.systemStateRepository.findOne(params);
    }

    async update(systemState: SystemState, params: object) {
        return await getConnection()
            .createQueryBuilder()
            .update(SystemState)
            .set(params)
            .where("id = :id", { id: systemState.id })
            .execute();
    }

    async findEmails(params?: object) {
        return await this.systemEmailRepository.find(params);
    }

    async findSupplierEmailRecipients(params?: object) {
        return await this.supplicerEmailRecipientRepository.find(params);
    }

    async saveEmails(emails: Array<any>, type: string) {
        await this.connection.query(`
                        DELETE FROM system_email_recipient WHERE type = '${type}'
                    `);

        emails.forEach(async email => {
            const email_recipient = new SystemEmailRecipient();
            email_recipient.email = email;
            email_recipient.type = type
            await this.systemEmailRepository.save(email_recipient);
        });
    }

    async createError(error: any) {

        const {  } = error;
        let logError = new LogError();
        logError.message = error.message;
        logError.purchase_order = error.purchase_order;
        logError.type = error.type;
        logError.reference_number = error.reference_number;
        logError.file_name = error.file_name;

        return await this.logErrorRepository.save(logError);

    }

    async findOneError(params?: object) {
        return await this.logErrorRepository.findOne(params);
    }

    async createSystemErrorLog(log: any) {
        const {} = log;

        let error_message = 'unknown error, please check log for details'
        if (log.error.toString()) {
            error_message = log.error.toString()
        }

        let errorLog = new SystemErrorLog();
        errorLog.name = log.name;
        errorLog.error_message = error_message;
        errorLog.escalated = false;
        errorLog.updated_at = new Date();
        errorLog.created_at = new Date();

        return await this.systemErrorLogRepository.save(errorLog);
    }
}
