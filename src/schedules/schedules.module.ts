import { Module } from '@nestjs/common';
import { ApiModule } from 'src/api/api.module';
import { XmlBuilderModule } from 'src/helpers/builders/xml/xml-builder.module';
import { SftpModule } from 'src/helpers/sftp/sftp.module';
import { ErplySuppliersModule } from 'src/modules/boms/erply-suppliers/erply-suppliers.module';
import { ErplyWarehousesModule } from 'src/modules/boms/erply-warehouses/erply-warehouses.module';
import { PurchaseOrdersModule } from 'src/modules/boms/purchase-orders/purchase-orders.module';
import { SuppliersModule } from 'src/modules/boms/suppliers/suppliers.module';
import { ErplyModule } from 'src/modules/erply/erply.module';
import { GetPurchaseOrdersService } from './erply/get-purchase-orders/get-purchase-orders.service';
import { TransferFilesService } from './sftp/transfer-files/transfer-files.service';
import { ReadFilesService } from './sftp/read-files/read-files.service';
import { ImportFilesService } from './sps/import-files/import-files.service';
import { SystemModule } from 'src/modules/system/system.module';
import { SyncSuppliersService } from './erply/sync-suppliers/sync-suppliers.service';
import { SyncWarehousesService } from './erply/sync-warehouses/sync-warehouses.service';
import { RetrievePurchaseOrdersService } from './erply/get-purchase-orders/retrieve-purchase-orders.service';
import { XmlReaderModule } from 'src/helpers/readers/xml-reader.module';
import { MailModule } from 'src/helpers/mail/mail.module';
import { SendEscalationService } from './escalation/send-escalation/send-escalation.service';
import { SendErrorEscalationService } from './escalation/send-error-escalation/send-error-escalation.service';
import { SendSystemEscalationService } from './escalation/send-system-escalation/send-system-escalation.service';
import { AutoAcceptedOrderService } from './erply/auto-accepted-orders/auto-accepted-orders.service';

@Module({
  imports: [SuppliersModule, PurchaseOrdersModule, SuppliersModule, ErplyModule, SftpModule, XmlBuilderModule, XmlReaderModule, ApiModule, SystemModule, ErplySuppliersModule, MailModule, ErplyWarehousesModule],
  providers: [
    GetPurchaseOrdersService,
    TransferFilesService,
    ReadFilesService,
    ImportFilesService,
    SyncSuppliersService,
    SyncWarehousesService,
    RetrievePurchaseOrdersService,
    SendEscalationService,
    SendErrorEscalationService,
    AutoAcceptedOrderService,
    SendSystemEscalationService
  ],
})
export class SchedulesModule { }
