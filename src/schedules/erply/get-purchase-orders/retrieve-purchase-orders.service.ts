import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { type } from 'os';
import { ErplyApiService } from 'src/api/erply-api/erply-api.service';
import { MailService } from 'src/helpers/mail/mail.service';
import { LineItemsService } from 'src/modules/boms/line-items/line-items.service';
import { LogItemsService } from 'src/modules/boms/log-items/log-items.service';
import { PurchaseOrdersService } from 'src/modules/boms/purchase-orders/purchase-orders.service';
import { SuppliersSerrvice } from 'src/modules/boms/suppliers/suppliers.service';
import { ErplyPurchaseOrderService } from 'src/modules/erply/erply-purchase-order/erply-purchase-order.service';
import { ErplySuppliersService } from 'src/modules/boms/erply-suppliers/erply-suppliers.service';
import { ErplyWarehousesService } from 'src/modules/boms/erply-warehouses/erply-warehouses.service';
import { SystemService } from 'src/modules/system/system.service';
import { Connection } from 'typeorm';
import * as moment from 'moment-timezone';


@Injectable()
export class RetrievePurchaseOrdersService {
    constructor(
        private connection: Connection,
        private SuppliersSerrvice: SuppliersSerrvice,
        private PurchaseOrdersService: PurchaseOrdersService,
        private ErplyPurchaseOrderService: ErplyPurchaseOrderService,
        private ErplySuppliersService: ErplySuppliersService,
        private ErplyWarehousesService: ErplyWarehousesService,
        private LogItemsService: LogItemsService,
        private LineItemsService: LineItemsService,
        private ErplyApiService: ErplyApiService,
        private SystemService: SystemService,
        private MailService: MailService,
    ) { }

    /**
     * @Cron task
     * Interval: 15 minutes
     * Notes:
     * Grabs all the configured suppliers in the internal DB
     * Calls Erply API to get all POs created/updated in the last 15 minutes
     * **if the PO does not already exist, log it in the system and create a log item
     * **if existing log_item with type "purchase_order" has been flagged as requires_update (default true), it grabs all the product lines and stores it in the log item
     * 
     */
    async retrievePucharseOrders() {
        let state = await this.SystemService.find({
            where: {
                name: 'pos_sync'
            }
        });

        await this.SystemService.update(state, {
            active: true,
            updated_at: new Date()
        })
        console.log("PO data sync started.");

        const client_codes = await this.connection.query(`SELECT DISTINCT client_code FROM supplier`);
        let po_promises = []

        for (const client_code of client_codes) {
            let suppliers = await this.SuppliersSerrvice.findAll({
                client_code: client_code.client_code
            });
            let ediPos = await this.getEdiPosForDataset(client_code, suppliers)

            if (ediPos == false) {
                await this.SystemService.update(state, {
                    active: false,
                    updated_at: new Date()
                })
                continue;
            }

            let tmp_promises = await this.producePOsyncPromises(ediPos, suppliers)

            if (tmp_promises.length > 0) {
                po_promises = po_promises.concat(tmp_promises)
            }
        }

        while (po_promises.length) {
            await Promise.all(po_promises.splice(0, 90)).then((values) => {
                // handle resolve.
                this.buildUpdateEdiStatusRequests(values).then((reqs) => {
                    this.ErplyPurchaseOrderService.bulkUpdateEdiStatus(reqs); // This is going to update edi_status attribute for all POs.
                });  
            }).catch((e) => {
                this.SystemService.createSystemErrorLog({name: 'pos_sync', error: e})
                console.log(e)
                this.SystemService.update(state, {
                    active: false,
                    updated_at: new Date()
                })
            });
        }
        await this.SystemService.update(state, {
            active: false,
            updated_at: new Date()
        })
        console.log('PO data sync finished.')
    }

    async createBuyerNotification(po: any, blanket?: any) {
        let notified = await this.SystemService.findOneError({
            where: {
                purchase_order: po.erply_po_id,
                reference_number: 'buyer_invalid_cancellation',
                type: 'buyer_notification'
            },
        })

        if (!notified) {
            await this.SystemService.createError({
                message: blanket ? 'PO ' + po.erply_po_id + ' has release orders attached' : 'Dispatch advice received for PO ' + po.erply_po_id ,
                purchase_order: po.erply_po_id,
                type: 'buyer_notification',
                reference_number: 'buyer_invalid_cancellation',
                file_name: '',
            })
        }
    }

    async getEdiPosForDataset(dataset: any, suppliers: any) {
        try {
            let pos: any = []
            let supplier_ids = suppliers.map(supplier => supplier.erply_supplier_id)
            let newPromisess = await this.ErplyPurchaseOrderService.index(supplier_ids, 1); // new POs - edi_status = 1.
            let changedPromisess = await this.ErplyPurchaseOrderService.index(supplier_ids, 3); // changed/updated POs - edi_status = 3.
            let cancelledPromisess = await this.ErplyPurchaseOrderService.cancelledIndex(supplier_ids); // cancelled POs in last hour.

            if (newPromisess.length > 0) {
                let po_records = await this.resolveEdiPoPromises(newPromisess)
                pos = pos.concat(po_records)
            }

            if (changedPromisess.length > 0) {
                let po_records = await this.resolveEdiPoPromises(changedPromisess)
                pos = pos.concat(po_records)
            }

            if (cancelledPromisess.length > 0) {
                let po_records = await this.resolveEdiPoPromises(cancelledPromisess)
                pos = pos.concat(po_records)
                // Filter edi POs.
                /*let po_records_remove_from_cancel = po_records.filter((po) => {
                    let cancelled_Edi_ststus = po.attributes.find(attribute => attribute.attributeName == 'edi_status');
                    if (cancelled_Edi_ststus && cancelled_Edi_ststus.attributeValue != '5') {
                        return po 
                    }else{
                        console.log("=======================cancelled po----------")
                        console.log(po) 
                    }
                })
                pos = pos.concat(po_records_remove_from_cancel)*/
            }

            // Filter edi POs.
            const ediPos = pos.filter((po) => {
                if (po.attributes.length > 0) {
                    if (po.attributes.find(attr => attr.attributeName === 'edi_status')) {
                        return po
                    }
                }
            })

            return ediPos
        } catch(e) {
            console.log(dataset.client_code)
            console.log(`error when trying to fetch POs from Erply API.` + moment().format('YYYY-MM-DD HH:mm:ss'))
            this.SystemService.createSystemErrorLog({name: 'pos_sync', error: e})
            console.error(e)
            return false
        }

    }

    async producePOsyncPromises(ediPos: Array<any>, suppliers: Array<any>) {
        let po_promises = []
        for (const po of ediPos) {
            po_promises.push(new Promise(async (resolve, reject) => {
                const edi_status = po.attributes.find(attribute => attribute.attributeName == 'edi_status');
                const earliest_delivery_date = new Date(po.shipDate ? po.shipDate : po.createdDate);
                const latest_delivery_date = po.attributes.find(attribute => attribute.attributeName == 'edi_latest_delivery_date');
                const blanket_id = po.attributes.find(attribute => attribute.attributeName == 'edi_blanket_order_number');
                const contract_number = po.attributes.find(attribute => attribute.attributeName == 'edi_contract_number');
                let type_attribute = po.attributes.find(attribute => attribute.attributeName == 'edi_po_type');
                const state = po.stateID;
                let send_status = 'ready_to_send';

                if (!latest_delivery_date || latest_delivery_date.attributeValue == "" || !earliest_delivery_date) {
                    send_status = 'incomplete_delivery_window'
                    //Send an email to Relevant buyers of the supplier
                    // let recipients = await this.SuppliersSerrvice.getRecipients(17956648);
                    // this.MailService.sendErrorEmail(recipients, `EDI: PO-${po.regnumber} missing delivery date information`, po.regnumber,  `EDI: PO-${po.regnumber} is missing delivery date window information`)
                }

                await this.SystemService.update(state, {
                    active: true
                })

                try { // try and catch error and handle it later.
                    //Check if PO already exists
                    let po_details = await this.PurchaseOrdersService.find({
                        where: { erply_po_id: po.regnumber },
                        relations: ['log_items']
                    });
                    const MESSAGE_FUNCTION = (state == 10 ? "CANCEL" : ((edi_status && edi_status.attributeValue == '3') ? 'REPLACE' : 'ORIGINAL'))

                    if (po_details && po_details.log_items.find(log => log.type == 'despatch_advice')) {
                        if (type_attribute.attributeValue.toUpperCase() == 'RELEASE' && state == 10 && po_details.status !== 'cancelled') {
                            await this.createBuyerNotification(po_details);
                        }

                        resolve({id: po.id, attributeName1: "edi_status",
                        attributeType1: "int",
                        attributeValue1: 5,
                        requestName: 'savePurchaseDocument'})
                        return;
                    }

                    if (po_details && type_attribute.attributeValue.toUpperCase() == 'BLANKET' && state == 10 && po_details.status !== 'cancelled') {
                        let find_blanket = await this.PurchaseOrdersService.find({
                            where: { erply_blanket_id: po.regnumber }
                        });
                        if (find_blanket) {
                            await this.createBuyerNotification(po_details, find_blanket);
                            resolve('send buyer notification')
                            return;
                        }
                    }


                    if(po_details && po_details.status === 'awaiting_response' && edi_status && edi_status.attributeValue == '1'){
                        console.log("============Existing po in awaiting status skipped==============");
                        console.log(`po number: `+po.regnumber+` -- Time: ` + moment().format('YYYY-MM-DD HH:mm:ss'));
                        resolve({id: po.id, attributeName1: "edi_status",
                        attributeType1: "int",
                        attributeValue1: 2,
                        requestName: 'savePurchaseDocument'})
                        return;
                    }

                    if (po_details && po_details.status === 'cancelled' && state == 10) {
                        // If found po already cancelled, should not try to cancel again.
                        resolve('cancelled already')
                        return
                    }

                    let address = await this.ErplySuppliersService.find({ where: {erply_supplier_id: po.supplierID}});
                    let warehouse = await this.ErplyWarehousesService.find({ where: {erply_warehouse_id: po.warehouseID}});

                    let warehouse_details = {
                        address: warehouse.street,
                        address2: warehouse.address2,
                        city: warehouse.city,
                        state: warehouse.state,
                        country: warehouse.country,
                        postcode: warehouse.postcode,
                        phone: warehouse.phone,
                        email: warehouse.email,
                    }
                    let supplier_details = {
                        address: address.address,
                        city: address.city,
                        state: address.state,
                        country: address.country,
                        postcode: address.postcode,
                        phone: address.phone,
                        email: address.email,
                    }

                     if (!po_details) {
                        let supplier = suppliers.find(supplier => supplier.erply_supplier_id == po.supplierID);

                        let type_attribute = po.attributes.find(attribute => attribute.attributeName == 'edi_po_type');
                        po_details = await this.PurchaseOrdersService.create({
                            erply_po_id: po.regnumber,
                            erply_base_id: po.id,
                            erply_no: po.number,
                            erply_blanket_id: blanket_id !== undefined? blanket_id.attributeValue : null,
                            supplier_id: supplier.id,
                            warehouse_id: po.warehouseID,
                            warehouse_name: (type_attribute && type_attribute.attributeValue.toUpperCase() == 'BLANKET') ? 'TAF HO' : po.warehouseName,
                            address: warehouse_details.address,
                            address2: warehouse_details.address2,
                            city: warehouse_details.city,
                            state: warehouse_details.state,
                            country: warehouse_details.country,
                            postcode: warehouse_details.postcode,
                            phone: warehouse_details.phone,
                            email: warehouse_details.email,
                            supplier_address: supplier_details.address,
                            supplier_city: supplier_details.city,
                            supplier_state: supplier_details.state,
                            supplier_country: supplier_details.country,
                            supplier_postcode: supplier_details.postcode,
                            supplier_phone: supplier_details.phone,
                            supplier_email: supplier_details.email,
                            type: type_attribute ? type_attribute.attributeValue.toUpperCase() : 'BLANKET',
                            status: (state == 10) ? 'cancelled' : send_status,
                            erply_created_at: po.lastModified,
                            erply_last_synced: po.lastModified,
                        })
                    } else {
                        let type_attribute = po.attributes.find(attribute => attribute.attributeName == 'edi_po_type');
                        let blanket_reference = po.attributes.find(attribute => attribute.attributeName == 'blanket_id');

                        await this.PurchaseOrdersService.update(po_details, {
                            address: warehouse_details.address,
                            city: warehouse_details.city,
                            state: warehouse_details.state,
                            country: warehouse_details.country,
                            postcode: warehouse_details.postcode,
                            phone: warehouse_details.phone,
                            email: warehouse_details.email,
                            supplier_address: supplier_details.address,
                            supplier_city: supplier_details.city,
                            supplier_state: supplier_details.state,
                            supplier_country: supplier_details.country,
                            supplier_postcode: supplier_details.postcode,
                            supplier_phone: supplier_details.phone,
                            supplier_email: supplier_details.email,
                            type: type_attribute !== undefined ? type_attribute.attributeValue.toUpperCase() : 'INDENT',
                            created_at: po.date,
                            status: (state == 10) ? 'cancelled' : send_status,
                            erply_last_synced: po.lastModified,
                        })
                    }

                    let log_item = await this.LogItemsService.create({
                        purchase_order_id: po_details.id,
                        reference_id: po.id ? po.id : null,
                        message_function: MESSAGE_FUNCTION,
                        contract_number: contract_number !== undefined ? contract_number.attributeValue : null,
                        notes: po.notes,
                        status: send_status,
                        type: "purchase_order",
                        total_lines: po.rows.length,
                        total_amount: po.rows.map(line => parseFloat(parseFloat(line.price).toFixed(2)) * parseInt(line.amount)).reduce((accumulator, a) => {
                            return accumulator + a;
                        }).toFixed(2),
                        total_amount_gst: po.rows.map(line => parseFloat(parseFloat(line.price).toFixed(2)) * parseInt(line.amount) * 1.1).reduce((accumulator, a) => {
                            return accumulator + a;
                        }).toFixed(2),
                        date: po.date,
                        delivery_earliest_date: earliest_delivery_date as any,
                        delivery_latest_date: latest_delivery_date !== undefined ? (latest_delivery_date.attributeValue as any) : null,
                    })

                    po.rows.forEach(async product => {
                        let code_array = product.code.split('-');
                        let size_code = code_array.pop();
                        let style_code = code_array.join('-')
                        let gtin = product.code2 ? product.code2.split(",")[0] : null;
                        let line_item = await this.LineItemsService.create({
                            log_id: log_item.id,
                            gtin: gtin,
                            product_id: product.productID,
                            style_code: style_code,
                            description: product.itemName,
                            colour_code: product.itemName,
                            size_code: size_code,
                            order_qty: product.amount,
                            unit_price: parseFloat(parseFloat(product.price).toFixed(2)),
                            tax_rate: parseFloat((parseFloat(product.price) * 0.1).toFixed(2)),
                        })
                    })

                    //2 Is status for sent
                    if (edi_status.attributeValue == '3' || edi_status.attributeValue == '1') {
                       resolve({id: po.id,  attributeName1: "edi_status", attributeType1: "int", attributeValue1:parseInt(edi_status.attributeValue) + 1, requestName: 'savePurchaseDocument'})
                    }
                    //Change the edi_status attribute of this PO to 1]

                    if (state == 10) {
                        // If found po already cancelled, should not try to cancel again.
                        resolve({id: po.id, attributeName1: "edi_status",
                        attributeType1: "int",
                        attributeValue1: 5,
                        requestName: 'savePurchaseDocument'})
                    }

                    resolve('unexpected');
                } catch (e) {
                    console.log(po)
                    reject(e);
                }
            }));
        }
        return po_promises
    }

    async buildUpdateEdiStatusRequests(reqs: any) {
        let returnReqs = []
        let reqCount = 1
        reqs.forEach((req) => {
            if (typeof(req) === 'object') {
                req.requestID = reqCount
                reqCount++;
                returnReqs.push(req)
            }
        })

        return returnReqs
    }

    async resolveEdiPoPromises(promises: Promise<any>[]) {
        const awaitTimeout = delay =>
            new Promise(resolve => setTimeout(resolve, delay));

        let records = []

        await Promise.all(promises).then((values) => {
            return awaitTimeout(3600 + Math.random() * 2000).then(function () { // timeout to avoid socket hang up and too many requests error.
                if (values.length > 0) {
                    values.forEach(value => {
                        if (value.status.errorCode == 0) {
                            records = records.concat(value.records)
                        } else {
                            console.log(value)
                        }
                    })
                }
            }).catch((e)=> {
                console.log(values)
                console.log(e)
            })
        })

        return records
    }
}
