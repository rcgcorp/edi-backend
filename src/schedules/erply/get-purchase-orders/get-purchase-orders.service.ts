import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { ErplyApiService } from 'src/api/erply-api/erply-api.service';
import { LineItemsService } from 'src/modules/boms/line-items/line-items.service';
import { LogItemsService } from 'src/modules/boms/log-items/log-items.service';
import { PurchaseOrdersService } from 'src/modules/boms/purchase-orders/purchase-orders.service';
import { SuppliersSerrvice } from 'src/modules/boms/suppliers/suppliers.service';
import { ErplyPurchaseOrderService } from 'src/modules/erply/erply-purchase-order/erply-purchase-order.service';
import { SystemService } from 'src/modules/system/system.service';
import { Connection } from 'typeorm';
import { RetrievePurchaseOrdersService } from './retrieve-purchase-orders.service';



@Injectable()
export class GetPurchaseOrdersService {
    constructor(
        private RetrievePurchaseOrdersService: RetrievePurchaseOrdersService,
        private SystemService: SystemService
    ) { }

    /**
     * @Cron task
     * Interval: 15 minutes
     * Notes:
     * Grabs all the configured suppliers in the internal DB
     * Calls Erply API to get all POs created/updated in the last 15 minutes
     * **if the PO does not already exist, log it in the system and create a log item
     * **if existing log_item with type "purchase_order" has been flagged as requires_update (default true), it grabs all the product lines and stores it in the log item
     * 
     */
    @Cron('*/15 * * * *')
    async sync() {
        try {
            let state = await this.SystemService.find({
                where: {
                    name: 'pos_sync'
                }
            });

            if(state.active) {
               console.log(
                   "Purchase Orders already Syncing in Erply"
               );
            }else {
                this.RetrievePurchaseOrdersService.retrievePucharseOrders();
            }
        } catch (e) {
            throw e;
        }
        
    }
}
