import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { ErplySuppliersService } from 'src/modules/boms/erply-suppliers/erply-suppliers.service';
import { ErplyAddressService } from 'src/modules/erply/erply-address/erply-address.service';
import { ErplySupplierService } from 'src/modules/erply/erply-supplier/erply-supplier.service';

@Injectable()
export class SyncSuppliersService {

    constructor(
        private ErplySupplierService: ErplySupplierService,
        private ErplySuppliersService: ErplySuppliersService,
        private ErplyAddressService: ErplyAddressService,
    ) { }

    @Cron('0 0 * * *')
    async syncSuppliers() {
        console.log("Suppliers are syncing")
        this.ErplySupplierService.index(3).subscribe(async response => {
            const pages = Math.ceil(response['status']['recordsTotal'] / 100);
            this.ErplySupplierService.bulkIndex(pages).subscribe(async inner_response => {
                const suppliers = [];
                inner_response['requests'].forEach(request => {
                    request['records'].forEach(async supplier => {
                        suppliers.push(supplier);

                    })
                })

                const supplier_ids = suppliers.map(supplier => supplier.supplierID);
                const pages = Math.ceil(supplier_ids.length / 100);
                const addresses = []
                for (let page = 0; page < pages; page++) {
                    const sliced_ids = supplier_ids.slice(page * 100, (page + 1) * 100)
                    const requests = (await this.ErplyAddressService.bulkIndex(sliced_ids).toPromise().catch(e => []))['requests']
                    // addresses.push(requests.map(request => request['records']));
                    requests.forEach(({ status, records }) => {
                        if (records.length)
                            addresses.push(records[0]);
                    })
                }

                suppliers.forEach(supplier => {
                    try {
                        const address_details: any = addresses.find(address => address.ownerID == supplier['supplierID']);
                        this.ErplySuppliersService.updateOrCreate(supplier['supplierID'], {
                            erply_supplier_id: supplier['supplierID'],
                            name: supplier['fullName'],
                            type: supplier['supplierType'],
                            group_id: supplier['groupID'],
                            // group_name: supplier['groupName'],
                            phone: supplier['phone'],
                            email: supplier['email'],
                            address: address_details ? address_details.street : null,
                            city: address_details ? address_details.city : null,
                            state: address_details ? address_details.state : null,
                            postcode: address_details ? address_details.postalCode : null,
                            country: address_details ? address_details.country : null,
                        });

                    } catch (err) {
                        console.log(err)
                    }
                })

            })
        })
    }
}
