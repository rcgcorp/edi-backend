import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { LogItemsService } from 'src/modules/boms/log-items/log-items.service';
import { LineItemsService } from 'src/modules/boms/line-items/line-items.service';
import { Connection } from "typeorm";

@Injectable()
export class AutoAcceptedOrderService {
    constructor(
        private LogItemsService: LogItemsService,
        private connection: Connection,
    ) { }

    @Cron('*/9 * * * *')
    async autoAccepted() {
        const accpted_orders_logitems = await this.connection.query(`
            SELECT log_item.id FROM log_item
            LEFT JOIN purchase_order ON purchase_order.id = log_item.purchase_order_id
            WHERE log_item.status ='change_accepted' AND purchase_order.status = 'change_accepted'
            ORDER BY purchase_order.erply_po_id DESC
            LIMIT 50
        `);

        if (accpted_orders_logitems.length > 0) {
            for (let i=0; i < accpted_orders_logitems.length; i++) {
                setTimeout(async () => {
                    var accepted_logitem = await this.LogItemsService.find({
                        where: {
                            id: accpted_orders_logitems[i].id
                        },
                        relations: ['purchase_order']
                    })
                    await this.LogItemsService.acceptEvent(accepted_logitem);
                }, 2000 * (i + 1)); // adding timeout to avoid hitting rate limit
            }
        }
    }
}