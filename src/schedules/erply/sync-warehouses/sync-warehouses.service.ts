import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { ErplyWarehousesService } from 'src/modules/boms/erply-warehouses/erply-warehouses.service';
import { ErplyWarehouseService } from 'src/modules/erply/erply-warehouse/erply-warehouse.service';


@Injectable()
export class SyncWarehousesService {
    constructor(
        private ErplyWarehouseService: ErplyWarehouseService,
        private ErplyWarehousesService: ErplyWarehousesService,
    ) { }

    @Cron('0 0 * * *')
    async syncSuppliers() {
        console.log("Warehouses are syncing")
        this.ErplyWarehouseService.index().subscribe(async response => {
            try {
                if (response['status'] && response['status']['errorCode'] == 0) {
                    response['records'].forEach(async warehouse => {
                        this.ErplyWarehousesService.updateOrCreate(warehouse.warehouseID, {
                            erply_warehouse_id: warehouse.warehouseID,
                            name: warehouse.name,
                            store_group: warehouse.storeGroups,
                            phone: warehouse.phone,
                            email: warehouse.email,
                            street: warehouse.street,
                            address2: warehouse.address2,
                            city: warehouse.city,
                            state: warehouse.state,
                            postcode:warehouse.postalCode ? warehouse.postalCode : warehouse.ZIPcode,
                            country: warehouse.country
                        })
                    })
                } else {
                    console.log(response)
                    console.log('failed to get warehouses')
                }
            } catch (err) {
                console.log(err)
            }
        })
    }
}