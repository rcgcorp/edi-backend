import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { XMLReaderDirector } from 'src/helpers/readers/xml-reader.director';
@Injectable()
export class ImportFilesService {
    constructor(
        private XmlReaderDirector: XMLReaderDirector,
    ) { }
    @Cron('*/7 * * * *')
    async importXMLFiles() {
        try {
            let fs = require('fs'), convert = require('xml-js');
            let directory = './temp/pending/'

            fs.readdir(directory, async (err, files) => {
                console.log("READING THE DIRECTORY", files);
                for (const file of files) {
                    try {
                        if (fs.existsSync(directory + file)) {
                        
                            let data = JSON.parse(convert.xml2json(fs.readFileSync(directory + file), { compact: true, spaces: 4 }));
                            let key = Object.keys(data)[1];

                            console.log("READING FILES!", key)

                            if (key == 'PurchaseOrderResponse') {
                                console.log("READING POR")
                                await this.XmlReaderDirector.readPOR(data, file);
                            }
                            if (key == 'DespatchAdvice') {
                                console.log("READING ASN")
                                await this.XmlReaderDirector.readASN(data, file);
                            }
                            if (key == 'Invoice') {
                                console.log("READING ASN")
                                await this.XmlReaderDirector.readINV(data, file);
                            }
                            console.log('==============file read complete ============='+directory + file);
                        }else{
                            console.log('==============file not exists============='+directory + file);
                        }
                    } catch (e) {
                        console.log(e);
                        let error_directory = './temp/error/'
                        fs.rename(directory + file, error_directory + file, function (err) {
                            if (err) throw err
                            console.log('Transfer of errored file success')
                        })
                    }
                }
        })
    } catch(e) {
        console.log(e);
    }
}

}
