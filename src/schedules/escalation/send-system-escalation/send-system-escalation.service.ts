import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { MailService } from 'src/helpers/mail/mail.service';
import { Connection } from 'typeorm';
import { SystemService } from 'src/modules/system/system.service';
import * as moment from 'moment-timezone';

@Injectable()
export class SendSystemEscalationService {
    constructor(
        private connection: Connection,
        private MailService: MailService,
        private SystemService: SystemService
    ) { }

    
    @Cron('0 * * * *')
    async escalateSystemPosSyncError() {
        let posSyncErrors = await this.connection.query(`
            SELECT id,name,error_message,DATE_FORMAT(updated_at,'%e/%c/%Y %h:%i:%s %p') as updated_at, DATE_FORMAT(created_at,'%e/%c/%Y %h:%i:%s %p') as created_at FROM system_error_log WHERE escalated = FALSE AND name = 'pos_sync'
        `);
        
        posSyncErrors = Object.values(JSON.parse(JSON.stringify(posSyncErrors)))
        console.log(posSyncErrors)
        await this.MailService.sendSystemStateErrorEmail(posSyncErrors)

        await this.connection.query(`
            UPDATE system_error_log sel
            SET sel.escalated = TRUE
            WHERE sel.escalated = FALSE AND name = 'pos_sync'
        `)
    }

    @Cron('*/20 * * * *')
    async escalateSystemStateError() {
        let state = await this.SystemService.find({
            where: {
                name: 'pos_sync',
                active: 1,
            }
        });

        if (moment.utc(state.updated_at, 'YYYY-MM-DD HH:mm:ss').tz('Australia/Sydney').isBefore(moment().tz('Australia/Sydney').subtract('00:20:00'))) {
            // If active field is not reset back to 0 in 20 minutes, send mail.
           await this.MailService.sendUnlockSystemStateEmail()
        }
    }
}