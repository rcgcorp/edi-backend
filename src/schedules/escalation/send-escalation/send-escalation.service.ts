import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { MailService } from 'src/helpers/mail/mail.service';
import { LogItemsService } from 'src/modules/boms/log-items/log-items.service';
import { Connection } from 'typeorm';

@Injectable()
export class SendEscalationService {
    constructor(
        private connection: Connection,
        private LogItemsService: LogItemsService,
        private MailService: MailService
    ) { }

    @Cron('*/11 * * * *')
    async escalateThresholdErrors() {
        let logItems = await this.connection.query(`
        SELECT po.erply_po_id, MAX(lo.reference_id) AS reference_id, lo.type, MAX(li.description) AS description, CONCAT(li.style_code, CONCAT('-', li.size_code)) AS style_code, li.order_qty,  MAX(po.erply_no) AS tracking_number,
        SUM( li.delivered_qty ) AS delivered_qty, li.unit_price, MAX(li.delivered_unit_price) AS delivered_unit_price, li.fails_quantity_threshold, li.fails_price_threshold, li.fails_missing_item, li.fails_unknown_item, MAX(lo.fails_date_threshold) AS fails_date_threshold, MAX(lo.message_function) AS rejected,
        lo.fails_header_threshold, lo.log_buyer_code,  lo.log_ship_to_code,  lo.log_supplier_code, MAX(li.gtin) AS gtin,
        '${process.env.TAF_BUYER_ID}' AS po_buyer, MAX(su.erply_supplier_id) AS po_supplier, MAX(po.warehouse_id) AS po_ship_to, su.id AS supplier_id
            FROM purchase_order po 
            RIGHT JOIN supplier su ON po.supplier_id = su.id
        LEFT JOIN log_item lo ON po.id = lo.purchase_order_id
        LEFT JOIN line_item li ON lo.id = li.log_id
        WHERE lo.escalated = FALSE AND 
        lo.type <> 'purchase_order' AND
        (lo.fails_date_threshold = TRUE OR li.fails_price_threshold OR li.fails_quantity_threshold OR li.fails_missing_item OR li.fails_unknown_item OR lo.fails_header_threshold OR lo.message_function = 'REJECTED')
        GROUP BY po.erply_po_id, lo.type, li.id

        `);
        logItems = Object.values(JSON.parse(JSON.stringify(logItems)))
        if (logItems.length) {
            console.log("LOG ITEMS TO ESCALATE ", logItems);
            await this.MailService.sendEscalationEmail(Object.values(JSON.parse(JSON.stringify(logItems))))
        }

        await this.connection.query(`
            UPDATE log_item lo
            LEFT JOIN line_item li ON lo.id = li.log_id
            SET lo.escalated = TRUE
            WHERE lo.escalated = FALSE AND 
            (lo.fails_date_threshold = TRUE OR li.fails_price_threshold OR lo.fails_header_threshold OR li.fails_quantity_threshold OR li.fails_missing_item OR li.fails_unknown_item OR lo.message_function = 'REJECTED')
        `)
    }
}

