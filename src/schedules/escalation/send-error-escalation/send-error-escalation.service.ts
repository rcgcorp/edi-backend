import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { MailService } from 'src/helpers/mail/mail.service';
import { LogItemsService } from 'src/modules/boms/log-items/log-items.service';
import { Connection } from 'typeorm';

@Injectable()
export class SendErrorEscalationService {
    constructor(
        private connection: Connection,
        private LogItemsService: LogItemsService,
        private MailService: MailService
    ) { }

    @Cron('*/10 * * * *')
    async escalateThresholdErrors() {
        let logErrors = await this.connection.query(`
            SELECT * from log_error WHERE escalated = FALSE AND type != 'buyer_notification'
        `);
        logErrors = Object.values(JSON.parse(JSON.stringify(logErrors)))
        console.log(logErrors);
        if (logErrors.length) {
            await this.MailService.sendErrorEscalationEmail(Object.values(JSON.parse(JSON.stringify(logErrors))))
        }

        await this.connection.query(`
            UPDATE log_error le
            SET le.escalated = TRUE
            WHERE le.escalated = FALSE AND type != 'buyer_notification'
        `)
    }

    @Cron('*/10 * * * *')
    async escalateBuyerNotifications() {
        let logErrors = await this.connection.query(`
            SELECT * from log_error WHERE escalated = FALSE AND type = 'buyer_notification'
        `);
        logErrors = Object.values(JSON.parse(JSON.stringify(logErrors)))

        if (logErrors.length) {
            await this.MailService.sendBuyerNotificationEmail(Object.values(JSON.parse(JSON.stringify(logErrors))))
        }

        await this.connection.query(`
            UPDATE log_error le
            SET le.escalated = TRUE
            WHERE le.escalated = FALSE AND type = 'buyer_notification'
        `)
    }
}

