import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import * as fs from 'fs';
import { LineItemsService } from 'src/modules/boms/line-items/line-items.service';
import { LogItemsService } from 'src/modules/boms/log-items/log-items.service';
import { PurchaseOrdersService } from 'src/modules/boms/purchase-orders/purchase-orders.service';
import { SuppliersSerrvice } from 'src/modules/boms/suppliers/suppliers.service';
import { ErplyPurchaseOrderService } from 'src/modules/erply/erply-purchase-order/erply-purchase-order.service';
import { XmlBuilderDirector } from 'src/helpers/builders/xml/xml-builder.director';
import { ErplyAddressService } from 'src/modules/erply/erply-address/erply-address.service';
import { Connection } from 'typeorm';
import { SftpService } from 'src/helpers/sftp/sftp.service';
import { SystemService } from 'src/modules/system/system.service';


@Injectable()
export class TransferFilesService {

    constructor(
        private connection: Connection,
        private SuppliersSerrvice: SuppliersSerrvice,
        private PurchaseOrdersService: PurchaseOrdersService,
        private ErplyPurchaseOrderService: ErplyPurchaseOrderService,
        private LogItemsService: LogItemsService,
        private LineItemsService: LineItemsService,
        private XmlBuilderDirector: XmlBuilderDirector,
        private SystemService: SystemService,
        private ErplyAddressService: ErplyAddressService,
        private SftpService: SftpService
    ) { }

    @Cron('*/4 * * * *')
    async sendPurchaseOrder() {
        let poSyncRunning = await this.SystemService.find({
            where: {
                name: 'pos_sync',
                active: true
            }
        }); // always check po sync is not running, to avoid unexpected outcome.

        let log_items = await this.LogItemsService.findAll({
            where: {
                status: 'ready_to_send'
            },
            order: {
                reference_id: "ASC"
            },
            relations: ['purchase_order', 'purchase_order.supplier', 'line_items'],
            take: 100,
        })

        if (log_items.length == 0 || typeof poSyncRunning !== 'undefined') {
            return null;
        }
        console.log("Number of log items found to be sent:", log_items.length);

        log_items.forEach(async log => {
            let po = log.purchase_order;
            const xml = await this.XmlBuilderDirector.buildPurchaseOrderXML(po,
                {
                    address: po.supplier_address,
                    city: po.supplier_city,
                    state: po.supplier_state,
                    postcode: po.supplier_postcode,
                    country: po.supplier_country,
                    email: po.supplier_email,
                    phone: po.supplier_phone
                },
                {
                    address: po.address,
                    address2: po.address2,
                    city: po.city,
                    state: po.state,
                    postcode: po.postcode,
                    country: po.country,
                    email: po.email,
                    phone: po.phone
                },
                log)
            const filename = `PO${po.erply_po_id}-${Math.round((new Date()).getTime() / 1000)}.xml`
            const filepath = `temp/${filename}`

            var dir = 'temp';
            try {
                if (!fs.existsSync(dir)) {
                    fs.mkdirSync(dir, { recursive: true });
                }
    
                fs.writeFile(
                    filepath,
                    xml,
                    error => {
                        if (error) {
                            console.log(error);
                            throw error;
                        }
                    }
                );
    
                await this.SftpService.put(filename);
                this.LogItemsService.update(log, {
                    status: 'sent'
                });
    
                let status = 'awaiting_response'
                if (log.message_function == 'CANCEL') {
                    status = 'cancelled'
                }
                this.PurchaseOrdersService.update(po, {
                    status: status
                })
            } catch (err) {
                console.log('error on uploading: ' + po.erply_po_id)
                console.log(err)
            }
        })
    }
}
