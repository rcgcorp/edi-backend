import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { LineItemsService } from 'src/modules/boms/line-items/line-items.service';
import { LogItemsService } from 'src/modules/boms/log-items/log-items.service';
import { PurchaseOrdersService } from 'src/modules/boms/purchase-orders/purchase-orders.service';
import { SuppliersSerrvice } from 'src/modules/boms/suppliers/suppliers.service';
import { ErplyPurchaseOrderService } from 'src/modules/erply/erply-purchase-order/erply-purchase-order.service';
import { XmlBuilderDirector } from 'src/helpers/builders/xml/xml-builder.director';
import { ErplyAddressService } from 'src/modules/erply/erply-address/erply-address.service';
import { Connection } from 'typeorm';
import { SftpService } from 'src/helpers/sftp/sftp.service';

@Injectable()
export class ReadFilesService {

    constructor(
        private SftpService: SftpService
    ) { }
      @Cron('*/3 * * * *')
      async getFiles() {
        let retrieve = await this.SftpService.get();
      }
}
