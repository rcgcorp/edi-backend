import { Entity, Column, PrimaryColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class ErplySupplier {
  @PrimaryColumn({
    type: 'bigint'
  })
  erply_supplier_id: number;

  @Column()
  name: string;

  @Column()
  type: string;

  @Column({
      type: "bigint"
  })
  group_id: number;

  @Column({
    nullable: true
  })
  group_name: string;

  @Column()
  phone: string;

  @Column()
  email: string;

  @Column({
    nullable: true
  })
  address: string;

  @Column({
    nullable: true
  })
  city: string;

  @Column({
    nullable: true
  })
  postcode: string;

  @Column({
    nullable: true
  })
  state: string;

  @Column({
    nullable: true
  })
  country: string;

  @CreateDateColumn({ type: "timestamp"})
  public last_erply_sync: Date;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  public created_at: Date;

  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  public updated_at: Date;
}