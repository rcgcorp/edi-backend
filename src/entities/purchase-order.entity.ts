import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Timestamp, ManyToOne, JoinColumn, OneToMany,Index } from 'typeorm';
import { LogItem } from './log-item.entity';
import { Supplier } from './supplier.entity';

@Entity()
@Index(['erply_po_id'], { unique: true  })
export class PurchaseOrder {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'bigint'
    })
    erply_po_id: number;

    @Column({
        type: 'bigint'
    })
    erply_base_id: number;

    @Column({
        nullable: true
    })
    erply_no: string;

    @Column({ nullable: true })
    erply_blanket_id: number;

    @Column({
        type: 'bigint'
    })
    supplier_id: number;

    @Column({
        type: 'bigint'
    })
    warehouse_id: number;

    @Column()
    warehouse_name: string;

    @Column({ nullable: true })
    contract_number: string;

    @Column({ nullable: true })
    address: string;

    @Column({ nullable: true })
    address2: string;

    @Column({ nullable: true })
    city: string;

    @Column({ nullable: true })
    state: string;

    @Column({ nullable: true })
    country: string;

    @Column({ nullable: true })
    postcode: string;

    @Column({ nullable: true })
    phone: string;

    @Column({ nullable: true })
    email: string;

    @Column({ nullable: true })
    supplier_address: string;

    @Column({ nullable: true })
    supplier_city: string;

    @Column({ nullable: true })
    supplier_state: string;

    @Column({ nullable: true })
    supplier_country: string;

    @Column({ nullable: true })
    supplier_postcode: string;

    @Column({ nullable: true })
    supplier_phone: string;

    @Column({ nullable: true })
    supplier_email: string;

    @Column()
    type: string;

    @Column()
    status: string;

    @Column()
    erply_created_at: Number;

    @Column()
    erply_last_synced: Number;

    @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
    public created_at: Date;

    @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
    public updated_at: Date;

    @ManyToOne(type => Supplier, supplier => supplier.purchase_orders)
    @JoinColumn({ name: "supplier_id" })
    supplier: Supplier;

    @OneToMany(type => LogItem, li => li.purchase_order)
    @JoinColumn({ name: "purchase_order_id" })
    log_items: LogItem[];

}