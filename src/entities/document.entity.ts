import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, Timestamp, ManyToMany, JoinTable } from 'typeorm';
import { Supplier } from './supplier.entity';

@Entity()
export class Document {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  public created_at: Date;

  @ManyToMany(() => Supplier, (supplier) => supplier.documents)
  @JoinTable({
    name: "supplier_documents_document"
  })
  suppliers: Supplier[];
}