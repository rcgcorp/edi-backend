import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Supplier } from './supplier.entity';

@Entity()
export class EmailRecipient {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  supplier_id: number;

  @Column()
  email: string;

  @ManyToOne(type => Supplier, supplier => supplier.email_recipients)
  @JoinColumn({ name: "supplier_id" })
  supplier: Supplier;

}