import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class ErplyWarehouse {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  erply_warehouse_id: string;

  @Column()
  name: string;

  @Column({
    nullable: true
  })
  store_group: string;

  @Column({
    nullable: true
  })
  phone: string;

  @Column({
    nullable: true
  })
  email: string;

  @Column({
    nullable: true
  })
  street: string;

  @Column({
    nullable: true
  })
  address2: string;

  @Column({
    nullable: true
  })
  city: string;

  @Column({
    nullable: true
  })
  postcode: string;

  @Column({
    nullable: true
  })
  state: string;

  @Column({
    nullable: true
  })
  country: string;

  @CreateDateColumn({ type: "timestamp"})
  public last_erply_sync: Date;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  public created_at: Date;

  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  public updated_at: Date;
}