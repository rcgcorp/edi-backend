import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, Timestamp, ManyToMany, JoinTable } from 'typeorm';
import { LogItem } from './log-item.entity';
import { Supplier } from './supplier.entity';

@Entity()
export class LogError {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: true
  })
  purchase_order: number;

  @Column({
    nullable: true
  })
  reference_number: string;

  @Column({
    nullable: true
  })
  file_name: string;

  @Column()
  message: string;

  @Column({
    nullable: true
  })
  type: string;

  @Column({
    default: false
  })
  escalated: boolean;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  public created_at: Date;
}