import { Entity, Column, PrimaryGeneratedColumn,Index, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity()
@Index("unique_erplycode_erplygtin_ap21gtin", ["erply_code", "erply_gtin", "ap21_gtin"], { unique: true })
@Index("unique_erplycode", ["erply_code"], { unique: true })
@Index("unique_erplygtin", ["erply_gtin"], { unique: true })
@Index("unique_ap21gtin", ["ap21_gtin"], { unique: true })
export class PackConversion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  erply_code: string;

  @Column()
  erply_gtin: string;

  @Column()
  erply_productid: string;

  @Column()
  ap21_gtin: string;

  @Column()
  ap21_code: string;

  @Column()
  ap21_pack_qty: number;

  @Column({ type: 'double precision' })
  ap21_cost: number;
}