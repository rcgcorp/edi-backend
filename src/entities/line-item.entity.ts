import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Timestamp, JoinColumn, ManyToOne } from 'typeorm';
import { LogItem } from './log-item.entity';

@Entity()
export class LineItem {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    log_id: number;

    @Column({nullable: true})
    product_id: number;

    @Column({nullable: true})
    gtin: string;

    @Column({nullable: true})
    style_code: string;

    @Column({nullable: true})
    description: string;

    @Column({nullable: true})
    sscc: string;

    @Column({
        nullable: true
    })
    colour_code: string;

    @Column({
        default: 'NA',
        nullable: true
    })
    size_code: string;

    @Column({nullable: true})
    order_qty: number;

    @Column({nullable: true})
    delivered_qty: number;

    @Column({nullable: true})
    backordered_qty: number;

    @Column({
        type: 'double',
        nullable: true
    })
    unit_price: number;

    @Column({
        type: 'double',
        nullable: true
    })
    delivered_unit_price: number;

    @Column({
        type: 'double',
        nullable: true
    })
    tax_rate: number;

    @Column({
        default: false
    })
    fails_quantity_threshold: boolean;

    @Column({
        default: false
    })
    fails_quantity_within_threshold: boolean;

    @Column({
        default: false
    })
    fails_price_threshold: boolean;

    @Column({
        default: false
    })
    fails_price_within_threshold: boolean;

    @Column({
        default: false
    })
    fails_backorder_threshold: boolean;

    @Column({
        default: false
    })
    fails_missing_item: boolean;

    @Column({
        default: false
    })
    fails_unknown_item: boolean;


    @Column({ type: "timestamp", nullable: true })
    backorder_date: Timestamp;


    @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
    public created_at: Date;

    @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
    public updated_at: Date;

    @ManyToOne(type => LogItem, li => li.line_items)
    @JoinColumn({ name: "log_id" })
    log_item: LogItem;
}