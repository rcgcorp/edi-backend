import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany, JoinColumn, ManyToMany, JoinTable } from 'typeorm';
import { Document } from './document.entity';
import { EmailRecipient } from './email-recipient.entity';
import { PurchaseOrder } from './purchase-order.entity';

@Entity()
export class Supplier {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'bigint'
  })
  erply_supplier_id: number;

  @Column({ default: 500 })
  client_code: number;

  @Column()
  name: string;

  @Column({
    nullable: true
  })
  alias: string;

  @Column({ default: 0 })
  quantity_variation: number;

  @Column({ default: 0 })
  price_variation: number;

  @Column({ nullable: true })
  sftp_protocol: string;

  @Column({ nullable: true })
  sftp_endpoint: string;

  @Column({ nullable: true })
  sftp_username: string;

  @Column({ nullable: true })
  sftp_password: string;

  @Column({ nullable: true })
  edi_client_code: string;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  public created_at: Date;

  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  public updated_at: Date;

  @OneToMany(type => PurchaseOrder, po => po.supplier_id)
  @JoinColumn({ name: "supplier_id" })
  purchase_orders: PurchaseOrder[];

  @OneToMany(type => EmailRecipient, rec => rec.supplier)
  @JoinColumn({ name: "supplier_id" })
  email_recipients: EmailRecipient[];

  @ManyToMany(() => Document, (document) => document.suppliers)
  @JoinTable({
    name: "supplier_documents_document"
  })
  documents: Document[];
}