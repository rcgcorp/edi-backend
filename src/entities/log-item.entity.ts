import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Timestamp, OneToMany, JoinColumn, ManyToOne } from 'typeorm';
import { LineItem } from './line-item.entity';
import { PurchaseOrder } from './purchase-order.entity';

@Entity()
export class LogItem {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    purchase_order_id: number;

    //Reference ID may be from ERPLY or SPS
    @Column({
        nullable: true
    })
    reference_id: string;

    @Column({ nullable: true })
    reference_log_id: number;

    @Column({ nullable: true })
    file_name: string;

    @Column({ nullable: true })
    message_function: string;

    @Column({ nullable: true })
    contract_number: string;

    @Column()
    status: string;

    @Column()
    type: string;

    @Column({
        nullable: true
    })
    notes: string;

    @Column({ nullable: true })
    total_lines: number;

    @Column({
        type: 'double',
        nullable: true
    })
    total_amount: number;

    @Column({
        type: 'double',
        nullable: true
    })
    total_amount_gst: number;

    @Column({
        default: false
    })
    fails_threshold: boolean;

    @Column({ type: "timestamp", nullable: true })
    date: Timestamp;

    @Column({ type: "timestamp", nullable: true })
    delivery_earliest_date: Timestamp;

    @Column({ type: "timestamp", nullable: true })
    delivery_latest_date: Timestamp;

    @Column({
        default: false
    })
    fails_date_threshold: boolean;

    @Column({
        default: false
    })
    fails_header_threshold: boolean;

    @Column({
        nullable: true
    })
    log_supplier_code: string;
    
    @Column({
        nullable: true
    })
    log_ship_to_code: string;

    @Column({
        nullable: true
    })
    log_buyer_code: string;
    

    @Column({
        default: false
    })
    escalated: boolean;


    /***
     * ASN related
     */

    @Column({
        nullable: true
    })
    split_or_complete: string;

    @Column({
        nullable: true
    })
    consignment_note_number: string;

    @Column({
        nullable: true
    })
    carrier_name: string;

    @Column({
        nullable: true
    })
    sscc: string;

    @Column({
        nullable: true
    })
    shipment_tracking_number: string;

    @Column({ type: "timestamp", nullable: true })
    ship_date: Timestamp;

    @Column({ type: "timestamp", nullable: true })
    estimated_delivery_date: Timestamp;


    @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
    public created_at: Date;

    @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
    public updated_at: Date;


    @ManyToOne(type => PurchaseOrder, po => po.log_items)
    @JoinColumn({ name: "purchase_order_id" })
    purchase_order: PurchaseOrder;

    @OneToMany(type => LineItem, li => li.log_item)
    @JoinColumn({ name: "log_id" })
    line_items: LineItem[];

    @ManyToOne(type => LogItem, po => po.parent_log)
    @JoinColumn({ name: "reference_log_id" })
    reference_log: LogItem;

    @OneToMany(type => LogItem, lo => lo.reference_log)
    @JoinColumn({ name: "log_id" })
    parent_log: LogItem[];
}