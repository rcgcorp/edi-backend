import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn } from 'typeorm';

@Entity()
export class SystemEmailRecipient {
  @PrimaryGeneratedColumn()
  id: number;


  @Column()
  email: string;

  //values: buyers, errors
  @Column({
    default: 'buyers'
  })
  type: string;



}